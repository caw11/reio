/* tocmfast.cc
 * contains routines relating to opening, importing and manipulating 
 * the boxes output by 21CMFAST
 * It also contains routines for statistically analysing the boxes.
 */
// std headers:

/* DEVELOPMENT NOTES:

Put all the statistics modules in their own class e.g. stat2pt.cc
and put in the stats repository. Generalise them so that they work
with any 3D box that has been indexed into a 1D array (as per
stat3D) 

Ultimately this should contain only routines specific to manuipulating
21CMFAST output.

Can the NEW_DIM structure etc be done away with... i.e. by setting up a different class for differently resolved boxes?

 */
#include <sstream>
#include <iostream> 

using namespace std;

// Bespoke headers:
#include "../USERFLAGS.h"
#include "TOCMFAST.h" 

 /***************************************************
                     INITIALISATION
 ***************************************************/

Tocmfast::Tocmfast(FILE *LOG, Cosmology *c1)
{
  char filename[500];
  DBUG = LOG;
  setR(0.0);
  c=c1;
  fprintf(DBUG, "USE_TS_IN_21CM = %i \n", USE_TS_IN_21CM);
  sprintf(filename, "mkdir Output_files");
  system(filename);
}
Tocmfast::~Tocmfast()
{
  //  cout << "Destructor called for Tocmfast" << endl;
}

 /***************************************************
          ROUTINES CONTROLLING IN/OUT OF BOXES
  ***************************************************/
int Tocmfast::importbox(char *argv, float *box, int boxtype, int Res)
{
  int indx;

  fprintf(stderr, "\nTocmfast::importbox: Filename read in as %s\n", argv);

  // Open the box pointer, informing me if you cannot:
  if (!(F = fopen(argv, "rb"))){
    fprintf(stderr, "\nTocmfast::importbox: unable to open box at %s\nAborting...\n", argv);
    free(box);
    return -1;
  }
  fprintf(stderr, "in import box resolution passed to be: %i\n", Res);

  // Now fill the box with data from the file
  for (int i=0; i<Res; i++){
    for (int j=0; j<Res; j++){
      for (int k=0; k<Res; k++){  
	indx=real1DIndex(i,j,k,Res);

	if (boxtype==3) {
	  if (SmoothedDensityBox==1) { // Treat box as normal unpadded
	    if (fread(&box[indx], sizeof(float), 1, F)!=1){
	      fprintf(stderr, "\nTocmfast::importbox:: Read error occured while reading box.\n");
	    fclose(F); free(box);
	    return -1;
	    }
	    if (DELTA==1) {
	      box[indx]-=1.0;
	      //fprintf(stderr, "\nTocmfast::importbox:: Converting DELTA box to delta\n");
	    }
	    if (box[indx] < -1.0){ //Check density field physical
	      fprintf(stderr, "Less than 0???\n");
	    }
	  }
	  else { //Treat box as padded
	    if (fread(&box[indx], sizeof(float), 1, F)!=1){
	      fprintf(stderr, "\nTocmfast::importbox:: Read error occured while reading box.\n");
	      fclose(F); free(box);
	      return -1;
	    }
	    if (DELTA==1)  {
	      box[indx]-=1.0;
	      //fprintf(stderr, "\nTocmfast::importbox:: Converting padded DELTA box to padded delta\n");
	    
	    }
	    if (box[indx] < -1.0){ //Check density field physical
	      fprintf(stderr, "Less than 0???\n");
	    }
	  }

	}
	else {
	  if (fread(&box[indx], sizeof(float), 1, F)!=1){
	    fprintf(stderr, "\nTocmfast::importbox:: Read error occured while reading box.\n");
	    fclose(F); free(box);
	    return -1;
	  }
	}
      }
    }
  }

  return 0;
}

void Tocmfast::exportBox(float *box, double nf, double z, int boxtype)
{
  // NOTE THIS NEEDS ADJUSTING TO SUPPORT A PADDED DENSITY BOX XPORT (AS FFT PADDED)
  char filename[500];
  int total_pixels;
  
  total_pixels = HII_TOT_NUM_PIXELS;

  if (boxtype==1) sprintf(filename, "xH_smoothing_R%.2f_nf%f_z%.2f_useTs%i_%i_%.0fMpc", getR(), nf, z, USE_TS_IN_21CM, HII_DIM, BOX_LEN);
  else if (boxtype==2) { 
    if (model_inst==0) 
      sprintf(filename, "deltaTb_smoothing_R%.2f_nf%f_z%.2f_useTs%i_%i_%.0fMpc", getR(), nf, z, USE_TS_IN_21CM, HII_DIM, BOX_LEN);
    else {
      sprintf(filename, "deltaTb_smoothing_R%.2f_nf%f_z%.2f_useTs%i_%i_%.0fMpc", getR(), nf, z, USE_TS_IN_21CM, NEW_DIM, BOX_LEN);
      total_pixels=getNEW_TOT_NUM_PIXELS();
    }
  }
  else sprintf(filename, "density_smoothing_R%.2f_nf%f_z%.2f_useTs%i_%i_%.0fMpc", getR(), nf, z, USE_TS_IN_21CM, HII_DIM, BOX_LEN);

  F = fopen(filename, "wb");
  fprintf(stderr, "\nWritting output box: %s\n", filename);
  if (mod_fwrite(box, sizeof(float)*total_pixels, 1, F)!=1){
    fprintf(stderr, "\nTocmfast::exportBox- Write error occured while writting box.\n");
  }

  fclose(F);

}

/* copyBox - Makes copy of real *source box arrays in real *target */
void Tocmfast::copyBox(float *target, float *source)
{

    for (int i=0; i<HII_DIM; i++){
      for (int j=0; j<HII_DIM; j++){
	for (int k=0; k<HII_DIM; k++){
	  target[HII_R_INDEX(i,j,k)] = source[HII_R_INDEX(i,j,k)];
	  }
	}
      }
    
}


int Tocmfast::variXtract(char *arg, int boxtype, int &Res, float &L) 
{
  char Fname[500], *token;
  double z, nf, Zeta, dummy;
  float aveT;
  /* variXtract - Get variables from filename of type= boxtype*/

  strcpy(Fname, arg);
  fprintf(DBUG, "\nTocmfast::variXtract: Filename read in as %s\n", Fname);

  if (boxtype>3){
    fprintf(stderr, "tocmfast.cc: incorrect value for boxtype\n Aborting...\n");
    return -1;
  }


  if (boxtype==1) { //xH box
    token = strtok(Fname, "f");
    nf = atof(strtok(NULL, "_")); // neutral fraction
    setnf(nf);

    token = strtok(NULL,"eff");
    Zeta = atof(strtok(token, "_")); //Zeta, ionising efficiency
    setZeta(Zeta);

    strcpy(Fname, arg);
    token = strtok(Fname,"z");
    z = atof(strtok(NULL, "_")); // Redshift
    setz(z);
    Res=atof(strtok(NULL, "_")); //Resolution of the box
    L=atof(strtok(NULL, "_")); //Length of box side
  }
  else if (boxtype==2) { //deltaT box

    token = strtok(Fname, "f");
    cout<<token<<endl;
    nf = atof(strtok(NULL, "_"));
    setnf(nf);
    
    strcpy(Fname, arg);
    token = strtok(Fname,"z");
    cout<<token<<endl;
    z = atof(strtok(NULL, "_")); // Redshift
    setz(z);

    strcpy(Fname, arg);
    token = strtok(Fname,"b");
<<<<<<< HEAD
    cout<<token<<endl;
    aveT=atof(strtok(NULL, "_"));
    cout<<"aveTb2 = "<<aveT<<endl;
    Res=atof(strtok(NULL, "_"));
    //Res=atof(strtok(NULL, "_")); //cw added to read in boxes generated by v1.12
    cout<<"Resolution = "<<Res<<endl;
    L=atof(strtok(NULL, "_"));
    cout<<"Box side = "<<L<<endl;
=======
    aveT = atof(strtok(NULL, "_"));
    dummy = atof(strtok(NULL, "_")); //cw this may not be required for some formats of the name
    Res = atof(strtok(NULL, "_"));
    L = atof(strtok(NULL, "_"));
>>>>>>> refs/remotes/origin/master

  }
  else { //density box
    token = strtok(Fname, "z");
    z  = atof(strtok(NULL, "_")); // Redshift
    //setnf(-1);
    if (DEBUG==3) fprintf(DBUG, "\nRedshift under analysis is z = %4.2f\n",z);
    //token = strcpy(Fname, arg);
    setz(z);
    Res=atof(strtok(NULL, "_"));
    L=atof(strtok(NULL, "_"));
  }
  //".....
  fprintf(stderr,"boxtype=%i (1=xH, 2=dT, 3=delta)\n Box resolution per side is %i and its box length %.2f Mpc\n Redshift = %.2f, neutral fraction=%.5f\n",boxtype, Res, L, getz(), getnf());
  if (  ( (Res<10) || (L<100) ) ||  (getz()<=0) || (  !( (0.0<getnf())&&(getnf()<=1.0) ) )  )
    {
    fprintf(stderr,"***************** ERROR**********************\n The values of that have been extracted from the filename seem dodgy. \n Check your boxname and boxtype match (see above), if they do then your filename format must be incompatible with tocmfast.cc.  \n");
    return -1;
    }
  //....."
  return 1;
}

long long unsigned Tocmfast::real1DIndex(int x,int y,int z, int Res)
{  
  return z+Res*(y + Res*x);
} 

/***************************************************
       ROUTINES FOR STATISTICAL ANALYSIS OF BOXES
***************************************************/

/***********************************************/
/*********** HARD-CODED RESOLUTION *************/
/***********************************************/
double Tocmfast::getRes()
{
  // This returns the co-moving angular resolution from the hard coded MWA_Instrumentals for the redshift of interest. This is the resolution of the telescope extrapolated to the redshift under examination.
  int i, n, index;
  double z[100],ang_res[100], dummy1[100],dummy2[100];
  FILE *fp;

  fp=fopen("/home/caw11/Dropbox/1_WorkingOn/6_Observation/MWA_Instrumentals", "r");

  if(fp==NULL){
    printf("can not open MWA_Instrumentals\n");
    exit(1);
  }
  n=0;
  // Count the number of elements in the array
  while (fscanf(fp, "%*f\t%*f\t%*f\t%*f")!=EOF) {
    n++;
  }
  rewind(fp);
  
  //Import the file contents
  for(i=0; i<n; i++){
    fscanf(fp, "%lf\t%lf\t%lf\t%lf", &z[i], &dummy1[i],&ang_res[i],&dummy2[i] );
  }
  index=(getz()-z[0])*4.0; //work out what index our redshift corresponds to
  return ang_res[index]; 
}



/***********************************************/
/**************** RESAMPLE *********************/
/***********************************************/

int Tocmfast::resamplebox(char *argv, float *realbox, float *samplebox, int boxtype)
{
  //This resamples a real box to the resolution specified by NEW_DIM in the user variables selection of TOCM.h

  int pixel_factor,i,j,k;
  // find factor of our resampled pixel size / original pixel size
  pixel_factor = HII_DIM/NEW_DIM +0.5;
  if (DEBUG<=1) fprintf(DBUG, "new resolution of box = %.0f Mpc\n",pixel_factor*BOX_LEN/(HII_DIM+0.0));
  cout<<"new dimension in resamplebox"<<NEW_DIM<<endl;
  //If we're reducing the box resolution then filter the box accordingly
  if (NEW_DIM != HII_DIM) {
    smoothbox(argv, realbox, L_FACTOR*BOX_LEN/(NEW_DIM+0.0), boxtype);

    // now resample the filtered box to our NEW_DIM
    for (i=0; i<NEW_DIM; i++){
      for (j=0; j<NEW_DIM; j++){
	for (k=0; k<NEW_DIM; k++){
	  samplebox[real1DIndex(i,j,k,NEW_DIM)] = 
	    *((float *)realbox + HII_R_INDEX(i*pixel_factor,j*pixel_factor,k*pixel_factor)); ////Nb. removed factor of 1/VOLUME from the original in init.c as the FFT has been dealt with in HII_filter. Also changed from indexing of FFT box to that of normal box.
	} 
      }
    }
   
  }
  else //for whatever reason we've choosen not to reduce resolution
    importbox(argv, realbox, boxtype, HII_DIM);


  return 0;
}

/***********************************************/
/*************** SMOOTHBOX *********************/
/***********************************************/

int Tocmfast::smoothbox(char *file, float *realbox,float R, int boxtype)
{
  fftwf_plan plan;
  fftwf_complex *box;
  unsigned long long ct;
  int x,y,z, i,j,k;

// Takes a density padded real box of HII_KSPACE_NUM_PIXELS or otherwise a real box of HII_DIM, reads it into *box (FFT padded box) converts it to delta from DELTA (= 1+delta) (if DELTA=1 in header) and smoothes it, then FFTs to *realbox a real unpadded box. Adapted from 21CMFAST.

/********************* INITIALIZATION **************************/
  setR(R);
  //allocate and read-in the density array
  box = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*HII_KSPACE_NUM_PIXELS);
  if (!box){
    fprintf(stderr, "Tocmfast::smoothbox -> Error allocating memory for box box\nAborting...\n");
    return -1;
  }
  // Read in data from file in argv
  F = fopen(file, "rb");
  
  if ( (boxtype==3) && !(SmoothedDensityBox==1) ) { //read in for a padded density box   
    fprintf(stderr, "Reading in FFT padded density box\n");
    if (mod_fread(box, sizeof(fftwf_complex)*HII_KSPACE_NUM_PIXELS, 1, F)!=1){
      fftwf_free(box); fclose(F);
      fprintf(stderr, "Tocmfast::smoothbox: unable to read-in file\nAborting\n");
      return -1;
    }
  }
  else {  
    //fprintf(stderr, "Reading in realbox of HII_DIM=%i\n & pass it into FFT padded box\n", HII_DIM);
    for (i=0; i<HII_DIM; i++){
      for (j=0; j<HII_DIM; j++){
	for (k=0; k<HII_DIM; k++){
	  
	  if (fread(&realbox[HII_R_INDEX(i,j,k)], sizeof(float), 1, F)!=1){
	    fprintf(stderr, "Tocmfast::smoothbox: Read error occured while reading box in passed argv.\n");
	    fclose(F); free(realbox);
	    return -1;
	  }

	  *((float *)box + HII_R_FFT_INDEX(i,j,k)) = (realbox[HII_R_INDEX(i,j,k)]); // Should this factor be included?? *VOLUME/(HII_TOT_NUM_PIXELS+0.0), I don't think so as I think the factor of volume is omitted because FFt back again below. Also below the factor of 1/(HII_TOT_NUM_PIXELS+0.0) is done;
	  
	}
      }
    }
    fclose(F);
  }
  
  //If you're playing with a density box then deal with the question of converting back from DELTA or not (DELTA=rho/<rho> instead of rho/<rho>-1) and check its physicality
  if (boxtype==3) {
    for (i=0; i<HII_DIM; i++){
      for (j=0; j<HII_DIM; j++){
	for (k=0; k<HII_DIM; k++){
	  if (DELTA==1) {
	    *((float *)box + HII_R_FFT_INDEX(i,j,k)) -= 1;
	    //fprintf(stderr, "\nTocmfast::importbox:: Converting padded DELTA box to padded delta box\n");
	    if (*((float *)box + HII_R_FFT_INDEX(i,j,k)) < -1.0){
	      fprintf(stderr, "Less than 0???\n");
	    }
	  }
	  else {
	    if (*((float *)box + HII_R_FFT_INDEX(i,j,k)) < -1.0){
	      fprintf(stderr, "Less than -1???\n");
	    }
	  }

	}
      }
    } //END loops over ijk 
  }

  /************  END INITIALIZATION ******************/
  if (R>0) {
  //convert to k-space to filter
    plan = fftwf_plan_dft_r2c_3d(HII_DIM, HII_DIM, HII_DIM, (float *)box, (fftwf_complex *)box, FFTW_ESTIMATE);
    fftwf_execute(plan);
    fftwf_destroy_plan(plan);
    fftwf_cleanup();
    /* remember to add the factor of VOLUME/TOT_NUM_PIXELS when converting 
     from real space to k-space
     Note: we will leave off factor of VOLUME, in anticipation of 
     the inverse FFT below */
    for (ct=0; ct<HII_KSPACE_NUM_PIXELS; ct++){
      box[ct] /= (HII_TOT_NUM_PIXELS+0.0);
    }
    // filter the field
    fprintf(stderr, "Tocmfast::smoothbox -> Read done, now filtering on scale R=%e Mpc\n", R);
    HII_filter(box, 0, R);
  
    // do the FFT to get a real box back
    fprintf(stderr, "Tocmfast::smoothbox -> begin fft\n");
    plan = fftwf_plan_dft_c2r_3d(HII_DIM, HII_DIM, HII_DIM, (fftwf_complex *)box, (float *)box, FFTW_ESTIMATE);
    fftwf_execute(plan);
    fftwf_destroy_plan(plan);
    fftwf_cleanup();
    fprintf(stderr, "Tocmfast::smoothbox -> end fft\n");
  } // End of filtering

  else fprintf(stderr,"\n**************** You have choosen not to smooth your density box ****************\n");

  //Now put our smoothed box into the realbox pointer that was passed in
  for (x=0; x<HII_DIM; x++){
    for (y=0; y<HII_DIM; y++){
      for (z=0; z<HII_DIM; z++){
	// get the filtered mass on this scale
	realbox[HII_R_INDEX(x,y,z)] = *((float *)box + HII_R_FFT_INDEX(x,y,z));
      }
    }
 }
  // deallocate memory
  fftwf_free(box);
  return 0;
}

int Tocmfast::smthsmplbox(float *realbox, float R, int boxtype)
{
  fftwf_plan plan;
  fftwf_complex *box;
  unsigned long long ct;
  int x,y,z, i,j,k;

  /* Smoothes a resampled box (do not use for the density box!) 
     Only differs from normal smooth box in index macros and 
     that it doesn't need to read in the box again.
   */

/********************* INITIALIZATION **************************/
  setR(R);
  //allocate and read-in the density array
  box = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*NEW_KSPACE_NUM_PIXELS);
  if (!box){
    fprintf(stderr, "Tocmfast::smoothbox -> Error allocating memory for box box\nAborting...\n");
    return -1;
  }
  fprintf(stderr, "pixels per side = %i", NEW_DIM);
  for (i=0; i<NEW_DIM; i++){
    for (j=0; j<NEW_DIM; j++){
      for (k=0; k<NEW_DIM; k++){

	*((float *)box + NEW_R_FFT_INDEX(i,j,k)) = (realbox[NEW_R_INDEX(i,j,k)]); 
      }
    }
  }

  /************  END INITIALIZATION ******************/
  if (R>0) {
  //convert to k-space to filter
    plan = fftwf_plan_dft_r2c_3d(NEW_DIM, NEW_DIM, NEW_DIM, (float *)box, (fftwf_complex *)box, FFTW_ESTIMATE);
    fftwf_execute(plan);
    fftwf_destroy_plan(plan);
    fftwf_cleanup();
    /* remember to add the factor of VOLUME/TOT_NUM_PIXELS when converting 
     from real space to k-space
     Note: we will leave off factor of VOLUME, in anticipation of 
     the inverse FFT below */
    for (ct=0; ct<NEW_KSPACE_NUM_PIXELS; ct++){
      box[ct] /= (NEW_TOT_NUM_PIXELS+0.0);
    }
    // filter the field
    fprintf(stderr, "Tocmfast::smoothbox -> Read done, now filtering on scale R=%e Mpc\n", R);
    NEW_filter(box, 0, R);
  
    // do the FFT to get a real box back
    fprintf(stderr, "Tocmfast::smoothbox -> begin fft\n");
    plan = fftwf_plan_dft_c2r_3d(NEW_DIM, NEW_DIM, NEW_DIM, (fftwf_complex *)box, (float *)box, FFTW_ESTIMATE);
    fftwf_execute(plan);
    fftwf_destroy_plan(plan);
    fftwf_cleanup();
    fprintf(stderr, "Tocmfast::smoothbox -> end fft\n");
  } // End of filtering

  else fprintf(stderr,"\n**************** You have choosen not to smooth your density box ****************\n");

  //Now put our smoothed box into the realbox pointer that was passed in
  for (x=0; x<NEW_DIM; x++){
    for (y=0; y<NEW_DIM; y++){
      for (z=0; z<NEW_DIM; z++){
	// get the filtered mass on this scale
	realbox[NEW_R_INDEX(x,y,z)] = *((float *)box + NEW_R_FFT_INDEX(x,y,z));
      }
    }
 }
  // deallocate memory
  fftwf_free(box);
  return 0;
}

/***********************************************/
/*************** POWERSPEC *********************/
/***********************************************/

int Tocmfast::PowerSpec(float *box1, float *box2, int boxtype1, int boxtype2, int dimensional) 
{
/*Calculates the power spectrum of two different real fields. boxtype is the same as for PowerSpec. Adapted from 21CMFAST. */

  char filename[500], psoutputdir[500];
  const char *type1, *type2;
  fftwf_complex *delta1, *delta2;
  fftwf_plan plan;
  float k_x, k_y, k_z, k_mag, k_floor, k_ceil, k_max, k_first_bin_ceil, k_factor;
  unsigned long long ct, *in_bin_ct;
  double ave1, ave2, *p_box, *k_ave;
  unsigned int i,j,k, n_x, n_y, n_z, NUM_BINS;

  //Get the average for the box  
  ave1 = averagebox(box1, boxtype1,0); 
  ave2 = averagebox(box2, boxtype2,0);
  if (DEBUG>=2) fprintf(DBUG, "\nIn Tocmfast::PowerSpec \n average1 = %f and everage2 = %f\n",ave1, ave2);

  k_factor = 1.5;
  k_first_bin_ceil = DELTA_K;
  k_max = DELTA_K*HII_DIM;
  // initialize arrays
  // ghetto counting (lookup how to do logs of arbitrary bases in c...)
  NUM_BINS = 0;
  k_floor = 0;
  k_ceil = k_first_bin_ceil;
  while (k_ceil < k_max){
    NUM_BINS++;
    k_floor=k_ceil;
    k_ceil*=k_factor;
  }

  p_box =  (double *)malloc(sizeof(double)*NUM_BINS);
  k_ave =  (double *)malloc(sizeof(double)*NUM_BINS);
  in_bin_ct = (unsigned long long *)malloc(sizeof(unsigned long long)*NUM_BINS);
  if (!p_box || !in_bin_ct || !k_ave){ // a bit sloppy, but whatever..
    fprintf(stderr, "Tocmfast::XPowerSpec: Error allocating memory.\nAborting...\n");
    fprintf(stderr, "Tocmfast::XPowerSpec.c: Error allocating memory.\nAborting...\n");
    free(box1); free(box2);
    return -1;
  }
  for (ct=0; ct<NUM_BINS; ct++){
    p_box[ct] = k_ave[ct] = 0;
    in_bin_ct[ct] = 0;
  }

  delta1 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*HII_KSPACE_NUM_PIXELS);
  delta2 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*HII_KSPACE_NUM_PIXELS);
  if (!delta1||!delta2) {
    fprintf(stderr, "Tocmfast::XPowerSpec: Unable to allocate memory for one of the delta boxes!\n");
    fprintf(stderr, "Tocmfast::XPowerSpec: Unable to allocate memory for one of the delta boxes!\n");
    free(box1); free(box2); free(p_box); free(k_ave); free(in_bin_ct);
    return -1;
  }

  // fill-up the real-space of the box
  for (i=0; i<HII_DIM; i++){
    for (j=0; j<HII_DIM; j++){
      for (k=0; k<HII_DIM; k++){

	if (boxtype1==3) {
	  *((float *)delta1 + HII_R_FFT_INDEX(i,j,k)) = (box1[HII_R_INDEX(i,j,k)])*VOLUME/(HII_TOT_NUM_PIXELS+0.0); 
	  //cout<<"[2]box1 IS a density file \n";
	}
	else {
	  *((float *)delta1 + HII_R_FFT_INDEX(i,j,k)) = (box1[HII_R_INDEX(i,j,k)]/ave1 - 1)*VOLUME/(HII_TOT_NUM_PIXELS+0.0);
	  //cout<<"[2]box1 is NOT a density file \n";
	}
	if (boxtype2==3) {
	  *((float *)delta2 + HII_R_FFT_INDEX(i,j,k)) = (box2[HII_R_INDEX(i,j,k)])*VOLUME/(HII_TOT_NUM_PIXELS+0.0);
	  //cout<<"[2]box2 IS a density file\n";
	}
	else {
	  *((float *)delta2 + HII_R_FFT_INDEX(i,j,k)) = (box2[HII_R_INDEX(i,j,k)]/ave2 - 1)*VOLUME/(HII_TOT_NUM_PIXELS+0.0);
	  //cout<<"[2]box2 is NOT a density file \n";
	}
	  if ( dimensional && !(boxtype1==3) && !(boxtype2==3) ){
	    //cout<<"[2]box1 AND box2 are NOT density files and the PS is dimensional\n";
	    *((float *)delta1 + HII_R_FFT_INDEX(i,j,k)) *= ave1;
	    *((float *)delta2 + HII_R_FFT_INDEX(i,j,k)) *= ave2;
	  }
	}
	// Note: we include the V/N factor for the scaling after the fft

      }
  } //END loops over i,j,k

  // transform to k-space
  plan = fftwf_plan_dft_r2c_3d(HII_DIM, HII_DIM, HII_DIM, (float *)delta1, (fftwf_complex *)delta1, FFTW_ESTIMATE);
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);
  fftwf_cleanup();

  plan = fftwf_plan_dft_r2c_3d(HII_DIM, HII_DIM, HII_DIM, (float *)delta2, (fftwf_complex *)delta2, FFTW_ESTIMATE);
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);
  fftwf_cleanup();

  // now construct the power spectrum file
  for (n_x=0; n_x<HII_DIM; n_x++){
    if (n_x>HII_MIDDLE)
      k_x =(n_x-HII_DIM) * DELTA_K;  // wrap around for FFT convention
    else
      k_x = n_x * DELTA_K;

    for (n_y=0; n_y<HII_DIM; n_y++){
      if (n_y>HII_MIDDLE)
	k_y =(n_y-HII_DIM) * DELTA_K;
      else
	k_y = n_y * DELTA_K;

      for (n_z=0; n_z<=HII_MIDDLE; n_z++){ 
	k_z = n_z * DELTA_K;
	
	k_mag = sqrt(k_x*k_x + k_y*k_y + k_z*k_z);

	// now go through the k bins and update
	ct = 0;
	k_floor = 0;
	k_ceil = k_first_bin_ceil;
	while (k_ceil < k_max){
	  // check if we fal in this bin
	  if ((k_mag>=k_floor) && (k_mag < k_ceil)){
	    in_bin_ct[ct]++;
	    p_box[ct] += pow(k_mag,3)*cabs(delta1[HII_C_INDEX(n_x, n_y, n_z)]*delta2[HII_C_INDEX(n_x, n_y, n_z)])/(2.0*PI*PI*VOLUME);
	    // note the 1/VOLUME factor, which turns this into a power density in k-space
	    k_ave[ct] += k_mag;
	    break;
	  }
	  ct++;
	  k_floor=k_ceil;
	  k_ceil*=k_factor;
	}
      }
    }
  } // end looping through k box

  // What type are the box whose PS we're making?
  type1=boxstring(boxtype1);
  type2=boxstring(boxtype2);

  sprintf(psoutputdir, "Output_files/%s%s_Power_spec", type1, type2);
  sprintf(filename, "mkdir %s", psoutputdir);
  system(filename);

  if (dimensional && !(boxtype1==3) && !(boxtype2==3)) {
    //cout<<"[3]box1 AND box2 are NOT density files AND the power spectrum is dimensional \n";
    sprintf(psoutputdir, "Output_files/%s%s_Power_spec/Dimensional",type1, type2);
  }
  else {
    //cout<<"[3]box1 OR box2 ARE density files OR the power spectrum is dimensionless or a combination \n";
    sprintf(psoutputdir, "Output_files/%s%s_Power_spec/Dimensionless",type1, type2);
  }  
  sprintf(filename, "mkdir %s", psoutputdir);
  system(filename);

  // now lets print out the k bins
  if (T_USE_VELOCITIES){
    if (USE_HALO_FIELD)
      sprintf(filename, "%s/%s%sPS_nf%f_z%.2f_useTs%i_zeta%.1e_%i_%.0fMpc_v%i", psoutputdir,type1, type2, getnf(), getz(), USE_TS_IN_21CM, HII_EFF_FACTOR, HII_DIM, BOX_LEN, VELOCITY_COMPONENT);
    else
      sprintf(filename, "%s/%s%sPS_no_halos_nf%f_z%.2f_useTs%i_zeta%.1e_%i_%.0fMpc_v%i", psoutputdir, type1, type2, getnf(), getz(), USE_TS_IN_21CM, HII_EFF_FACTOR, HII_DIM, BOX_LEN, VELOCITY_COMPONENT);
  }
  else {
    if (USE_HALO_FIELD)
      sprintf(filename, "%s/%s%sPS_nov_nf%f_z%.2f_useTs%i_zeta%.1e_%i_%.0fMpc", psoutputdir, type1, type2, getnf(), getz(), USE_TS_IN_21CM, HII_EFF_FACTOR, HII_DIM, BOX_LEN);
    else
      sprintf(filename, "%s/%s%sPS_nov_no_halos_nf%f_z%.2f_useTs%i_zeta%.1e_%i_%.0fMpc", psoutputdir, type1, type2, getnf(), getz(), USE_TS_IN_21CM, HII_EFF_FACTOR, HII_DIM, BOX_LEN);
  }

  F = fopen(filename, "w");
  if (!F){
    fprintf(stderr, "Tocmfast::XPowerSpec.c: Couldn't open file %s for writting!\n", filename);
    fprintf(stderr, "Tocmfast::XPowerSpec.c: Couldn't open file %s for writting!\n", filename);
    free(box1); free(box2); free(p_box); free(k_ave); free(in_bin_ct); fftwf_free(delta1); fftwf_free(delta2);
  }

  for (ct=1; ct<NUM_BINS; ct++){
    if (in_bin_ct[ct]>0)
      fprintf(F, "%e\t%e\t%e\n", k_ave[ct]/(in_bin_ct[ct]+0.0), p_box[ct]/(in_bin_ct[ct]+0.0), p_box[ct]/(in_bin_ct[ct]+0.0)/sqrt(in_bin_ct[ct]+0.0));
  }
  fclose(F); free(p_box); free(k_ave); free(in_bin_ct); fftwf_free(delta1); fftwf_free(delta2);
  return 0;
}

int Tocmfast::corrbox(float *box1, float *box2, int boxtype1, int boxtype2)
{

  /* Spatial correlation function of two boxes; 
     pass the same box if you wish to calculate auto-correlation. 
     IMPORTANT! Density boxes must be passed as real-unpadded boxes,
     i.e. imported via smoothbox rather than importbox

     Even this is slow so it is only designed to work with resampled
     boxes. If your simulation is pretty small already you can just 
     set 
  */
  char filename[500], outputdir[500];
  const char *type1, *type2;
  int index, start_pix, xtmp, ytmp, ztmp, bin_ct (0), *in_bin_ct,*R_ave, Rtmp, Rmax;
  double *corr;
  float ave1, ave2; 
  
  /*
   * INITIALISATION
   */
  // Allocate adequate memory to our pointers
  corr =  (double *)malloc(sizeof(double)*HII_TOT_NUM_PIXELS);
  R_ave = (int *)malloc(sizeof(int)*HII_TOT_NUM_PIXELS);
  in_bin_ct = (int *)malloc(sizeof(int)*HII_TOT_NUM_PIXELS);

  // What type are the boxes whose correlation we're taking?
  type1=boxstring(boxtype1);
  type2=boxstring(boxtype2);

  sprintf(outputdir, "Output_files/%s%s_2ptCorrelation/",type1, type2);
  
  sprintf(filename, "mkdir %s", outputdir);
  system(filename);
  
  sprintf(filename, "%s/%s%s2ptCorr_nf%f_z%.2f_useTs%i_zeta%.1e_%i_%.0fMpc", outputdir, type1, type2, getnf(), getz(), USE_TS_IN_21CM, HII_EFF_FACTOR, HII_DIM, BOX_LEN);
  
  F = fopen(filename, "w");
  if (!F){
    fprintf(stderr, "Tocmfast::corrbox: Couldn't open file %s for writting!\n", filename);
    free(box1); free(box2); free(corr); free(R_ave); free(in_bin_ct); 
  }

  //convert the boxes to be x/<x>-1 (not necessary for density box)
  //Get the average for the boxes  
  ave1 = averagebox(box1, boxtype1,0); 
  ave2 = averagebox(box2, boxtype2,0);

  Rmax=Rxyz2(HII_DIM,HII_DIM,HII_DIM);

  for (int i=0; i<HII_DIM; i++) {
    for (int j=0; j<HII_DIM; j++) {
      for (int k=0; k<HII_DIM; k++) {
        if (boxtype1<3)
          box1[HII_R_INDEX(i,j,k)] = (box1[HII_R_INDEX(i,j,k)]/ave1 - 1);
        if (boxtype2<3)
          box2[HII_R_INDEX(i,j,k)] = (box2[HII_R_INDEX(i,j,k)]/ave2 - 1);
      }
    }
  }


  /*
   * INTERESTING SECTION
   */

  //cycle through the box and work out the correlation using each pixel in turn as the reference point
  for (int i=0; i<HII_DIM; i++) {
    for (int j=0; j<HII_DIM; j++) {
      for (int k=0; k<HII_DIM; k++) {

	cout<<"in loop over reference box:"<<i<<"\t"<<j<<"\t"<<k<<endl;
	for (int x=0; x<HII_DIM; x++) {
	  for (int y=0; y<HII_DIM; y++) {
	    for (int z=0; z<HII_DIM; z++) {

	      //cout<<i<<"\t"<<j<<"\t"<<k<<"\t"<<x<<"\t"<<y<<"\t"<<z<<endl;
	      //Rtmp = Rxyz(i, j, k,x+0.0,y+0.0,z+0.0);
	      //Rtmp = Rxyz2((i-x+0.0), (j-y+0.0), (k-z+0.0));
	      Rtmp = (i-x)*(i-x);
	      Rtmp+= (j-y)*(j-y); 
	      Rtmp+= (k-z)*(k-z);
	      index = (int)((Rtmp+0.0)/(Rmax+0.0)+0.5);
	      R_ave[index]+=Rtmp;
	      corr[index]+=box1[HII_R_INDEX(i,j,k)]*box2[HII_R_INDEX(x,y,z)];
	      in_bin_ct[index]+=1;

	    }
	  }
	} //end xyz loop
	  
	
      }
    }
  } //end ijk loop

  //ghetto count my bin number
  for (int i=0; i<HII_TOT_NUM_PIXELS; i++) {
    if (in_bin_ct[i]>=1)
      bin_ct+=1;
  }
  cout<<"bin count="<<bin_ct<<endl;

  //output the correlation function to the output file
  for (int i=0; i<bin_ct; i++) { 
        fprintf(F, "%e\t%e\t%e\n", R_ave[i]/(in_bin_ct[i]+0.0), corr[i]/(in_bin_ct[i]+0.0)/(HII_TOT_NUM_PIXELS+0.0), corr[i]/(in_bin_ct[i]+0.0)/sqrt(in_bin_ct[i]+0.0)/(HII_TOT_NUM_PIXELS+0.0));
  }
  free(corr); free(R_ave); free(in_bin_ct);
  return 1;
}

int Tocmfast::corrbox_full(float *box1, float *box2, int boxtype1, int boxtype2)
{

  /* Spatial correlation function of two boxes; 
     pass the same box if you wish to calculate auto-correlation. 
     IMPORTANT! Density boxes must be passed as real-unpadded boxes,
     i.e. imported via smoothbox rather than importbox
     This is the "proper method" that calculates for a properly padded box. 
     Takes FOREVER, so made corrbox which doesn't bother with padding.  
  */
  char filename[500], outputdir[500];
  const char *type1, *type2;
  int index, maxSep, xtmp, ytmp, ztmp, *R_ave, Rtmp;

  unsigned long long bin_ct (0), *in_bin_ct;
  double ave1, ave2, *corr;
  
  
  /*
   * INITIALISATION
   */
  // Allocate adequate memory to our pointers
  corr =  (double *)malloc(sizeof(double)*HII_TOT_NUM_PIXELS);
  R_ave = (int *)malloc(sizeof(int)*HII_TOT_NUM_PIXELS);
  in_bin_ct = (unsigned long long *)malloc(sizeof(unsigned long long)*HII_TOT_NUM_PIXELS);

  maxSep = Rxyz2(HII_DIM,HII_DIM,HII_DIM);

  // What type are the boxes whose correlation we're taking?
  type1=boxstring(boxtype1);
  type2=boxstring(boxtype2);

  sprintf(outputdir, "Output_files/%s%s_2ptCorrelation/",type1, type2);
  
  sprintf(filename, "mkdir %s", outputdir);
  system(filename);
  
  sprintf(filename, "%s/%s%s2ptCorr_nf%f_z%.2f_useTs%i_zeta%.1e_%i_%.0fMpc", outputdir, type1, type2, getnf(), getz(), USE_TS_IN_21CM, HII_EFF_FACTOR, HII_DIM, BOX_LEN);
  
  F = fopen(filename, "w");
  if (!F){
    fprintf(stderr, "Tocmfast::corrbox: Couldn't open file %s for writting!\n", filename);
    free(box1); free(box2); free(corr); free(R_ave); free(in_bin_ct); 
  }
  //convert the boxes to be x/<x>-1 (not necessary for density box)
  //Get the average for the boxes  
  ave1 = averagebox(box1, boxtype1,0); 
  ave2 = averagebox(box2, boxtype2,0);

  for (int i=0; i<HII_DIM; i++) {
    for (int j=0; j<HII_DIM; j++) {
      for (int k=0; k<HII_DIM; k++) {
        if (boxtype1<3)
          box1[HII_R_INDEX(i,j,k)] = (box1[HII_R_INDEX(i,j,k)]/ave1 - 1);
        if (boxtype2<3)
          box2[HII_R_INDEX(i,j,k)] = (box2[HII_R_INDEX(i,j,k)]/ave2 - 1);
      }
    }
  }


  /*
   * INTERESTING SECTION
   */

  //cycle through the box and work out the correlation using each pixel in turn as the reference point
  for (int i=0; i<HII_DIM; i++) {
    for (int j=0; j<HII_DIM; j++) {
      for (int k=0; k<HII_DIM; k++) {
	// Now cylcle through 9 * our box gridded (for wrap round), we start from the reference pixel co-ordinates in the starting box as this is the largest distance we're intersted
// cw Nb. it would be worth terminating these loops at the max distance we care about for each co-ordinate.
// cw Nb. this loses accuracy for separations more than a box side as we'll miss some modes.
	cout<<"in loop over reference box:"<<i<<"\t"<<j<<"\t"<<k<<endl;
	for (int x=i; x<(3.0*HII_DIM); x++) {
	  for (int y=j; y<(3.0*HII_DIM); y++) {
	    for (int z=k; z<(3.0*HII_DIM); z++) {
	      //cout<<i<<"\t"<<j<<"\t"<<k<<"\t"<<x<<"\t"<<y<<"\t"<<z<<endl;
	      //Rtmp = Rxyz2(i+HII_DIM, j+HII_DIM, k+HII_DIM,x+0.0,y+0.0,z+0.0);
	      Rtmp = Rxyz2((i-x+0.0), (j-y+0.0), (k-z+0.0));

	      index = (int)((Rtmp+0.0)/(maxSep+0.0)+0.5);
	      R_ave[index]+=Rtmp;
		// adjust padded indices so that they are within box bounds
	      if ( (HII_DIM <= x)&&(x < 2.0*HII_DIM) )
		xtmp = x - HII_DIM;
	      else if (x >= 2.0*HII_DIM)
		xtmp = x - (2.0*HII_DIM);
	      else
		xtmp = x;

	      if ( (HII_DIM <= y)&&(y < 2.0*HII_DIM) )
		ytmp = y - HII_DIM;
	      else if (y >= 2.0*HII_DIM)
		ytmp = y - (2.0*HII_DIM);
	      else
		ytmp = y;
	      
	      if ( (HII_DIM <= z)&&(z < 2.0*HII_DIM) )
		ztmp = z - HII_DIM;
	      else if (z >= 2.0*HII_DIM)
		ztmp = z - (2.0*HII_DIM);
	      else
		ztmp = z;
	      
	      if ( (xtmp<0)||(xtmp>599)||(ytmp<0)||(ytmp>599)||(ztmp<0)||(ztmp>599) )
		cout<<"YIKES, something has gone wrong with our index reassignment x,y,z are being alled as: "<<xtmp<<"\t,"<<ytmp<<"\t,"<<ztmp<<"\t,for index= "<<index<<endl; 
	      
	      corr[index]+=box1[HII_R_INDEX(i,j,k)]*box2[HII_R_INDEX(xtmp,ytmp,ztmp)];
	      in_bin_ct[index]+=1;
	      

	    }
	  }
	}
	
      }
    }
  }

  //ghetto count my bin number
  for (int i=0; i<HII_TOT_NUM_PIXELS; i++) {
    if (in_bin_ct[i]>=1)
      bin_ct+=1;
  }
  cout<<bin_ct<<endl;

  //output the correlation function to the output file
  for (int i=0; i<bin_ct; i++) { 
    fprintf(F, "%e\t%e\t%e\n", sqrt(R_ave[i])/(in_bin_ct[i]+0.0), corr[i]/(in_bin_ct[i]+0.0)/(HII_TOT_NUM_PIXELS+0.0), corr[i]/(in_bin_ct[i]+0.0)/sqrt(in_bin_ct[i]+0.0)/(HII_TOT_NUM_PIXELS+0.0));
  }
  free(corr); free(R_ave); free(in_bin_ct);
  return 1;
}

//double Tocmfast::Rxyz(double i, double j, double k, double x, double y, double z)
int Tocmfast::Rxyz2(int sep1, int sep2, int sep3)
{
  /*returns the square of the euclidean separation of two points in 3D.
    Point 1 has co-ordinates i,j,k and Point 2 x,y,z
   */
  double R;
  
  R=(sep1*sep1) + (sep2*sep2) +(sep3*sep3);

  return R;
}

/***********************************************/
/*************** HISTOBOX *********************/
/***********************************************/
int Tocmfast::histobox_updated(float *box, int boxtype)
{

  // updated to allow for negative pixels 30/04/2014
  float *delta_m;//, M, floor, ciel, del, R;
  const char *type;
  double MAX, MIN;
  unsigned long long ct, *in_bin_ct; //index,
  FILE *F;
  unsigned int x,y,z, NUM_BINS;
  char filename[600],outputdir[600];
  double *p_box, *bin_ave, value; //ave,
  float  bin_floor, bin_ceil;


  // allocate memory for the filtered histogram
  delta_m = (float *)malloc(sizeof(float)*HII_TOT_NUM_PIXELS);
  if (!delta_m){
    fprintf(stderr, "\n In Tocmfast::histobox\n Error allocating memory for histogram array\nAborting...\n");
    free(box);free(delta_m);
    return -1;
  }

    // copy the delta values over to the histogram array
  MIN = 1000;
  MAX = -1000;
  for (x=0; x<HII_DIM; x++){
    for (y=0; y<HII_DIM; y++){
      for (z=0; z<HII_DIM; z++){
	// get the filtered mass on this scale
	//delta_m[HII_R_INDEX(x,y,z)] = pow(*((float *)box + HII_R_FFT_INDEX(x,y,z)), 2);
	//delta_m[HII_R_INDEX(x,y,z)] = *((float *)box + HII_R_FFT_INDEX(x,y,z)); CW edit
	delta_m[HII_R_INDEX(x,y,z)] = box[HII_R_INDEX(x,y,z)];
	if (delta_m[HII_R_INDEX(x,y,z)] < MIN){ MIN = delta_m[HII_R_INDEX(x,y,z)];}

	if (delta_m[HII_R_INDEX(x,y,z)] > MAX){ MAX = delta_m[HII_R_INDEX(x,y,z)];}
      }
    }
  }

  fprintf(stderr, "\n In Tocmfast::histobox\n Min = %.2f, Max = %.2f\n", MIN, MAX);

  // ghetto counting (lookup how to do logs of arbitrary bases in c...)
  NUM_BINS = 0;
  bin_floor = 0;
  bin_ceil = bin_first_bin_ceil;
  while (bin_ceil < MAX*bin_factor){ //cw change bin_max to MAX*bin_factor
    NUM_BINS++;
    bin_floor=bin_ceil;
    bin_ceil*=bin_factor;
  }
  if (MIN<0) { //cw addition start
    bin_ceil = 0;
    bin_floor = -1.0*bin_first_bin_ceil;
    while ( bin_floor > MIN*bin_factor) {
      NUM_BINS++;
      bin_ceil = bin_floor;
      bin_floor*=bin_factor;
    }
    
  }//cw addition end

  p_box =  (double *)malloc(sizeof(double)*NUM_BINS);
  bin_ave =  (double *)malloc(sizeof(double)*NUM_BINS);
  in_bin_ct = (unsigned long long *)malloc(sizeof(unsigned long long)*NUM_BINS);
  if (!p_box || !in_bin_ct || !bin_ave){ // a bit sloppy, but whatever..
    fprintf(stderr, "\n In Tocmfast::histobox\n Error allocating memory.\nAborting...\n");
    fftwf_free(box);
    return -1;
  }
  for (ct=0; ct<NUM_BINS; ct++){
    p_box[ct] = bin_ave[ct] = 0;
    in_bin_ct[ct] = 0;
  }

  for (x=0; x<HII_DIM; x++){
    for (y=0; y<HII_DIM; y++){
      for (z=0; z<HII_DIM; z++){
	value = delta_m[HII_R_INDEX(x,y,z)];
	/*if (value < 0)
	  value = 0;cw removed to allow negative pixels*/
	// now go through the k bins and update
	ct = 0;
	bin_floor = 0;
	bin_ceil = bin_first_bin_ceil;
	while (bin_ceil < MAX*bin_factor){ //cw change bin_max to MAX*bin_factor
	  // check if we fal in this bin
	  if ((value>=bin_floor) && (value < bin_ceil)){
	    in_bin_ct[ct]++;
	    bin_ave[ct] += value;
	    break;
	  }

	  ct++;
	  bin_floor=bin_ceil;
	  bin_ceil*=bin_factor;
	}
	if (MIN<0) { //cw addition start
	  bin_ceil = 0;
	  bin_floor = -1.0*bin_first_bin_ceil;
	  while ( bin_floor > MIN*bin_factor) {
	    if ((value>=bin_floor) && (value < bin_ceil)){
	      in_bin_ct[ct]++;
	      bin_ave[ct] += value;
	      break;
	    }
	    ct++;
	    bin_ceil = bin_floor;
	    bin_floor*=bin_factor;
	  }    
	}//cw addition end
      }
    }
  } // end loop through box

  /****************** CALCULATIONS OVER *************************/
  /******************* OUTPUT COMMENCES *************************/
  type=boxstring(boxtype); //gets a string appropriate to the boxtype
  sprintf(filename, "mkdir Output_files/Histograms/");  
  system(filename);
  sprintf(outputdir, "Output_files/Histograms/%s/",type);
  sprintf(filename, "mkdir %s", outputdir);
  system(filename);

  sprintf(filename, "%s%sHist_z%.2f_smoothingR%.2f_nf%f_%i_%.0fMpc",outputdir,type,getz(),getR(), getnf(), HII_DIM, BOX_LEN);
  F = fopen(filename, "w");
  if (!F){
    fprintf(stderr, "\n In Tocmfast::histobox\n Unable to open file: %s for writing\nCheck that the variXtract routines have been called \nAborting...\n", filename);
    fftwf_free(box);
    return -1;
  }
  // now print to the file
  for (ct=0; ct<NUM_BINS; ct++){
    value = bin_ave[ct]/(double)in_bin_ct[ct];
    fprintf(F, "%e\t%e\n", value, in_bin_ct[ct]/(double)HII_TOT_NUM_PIXELS/(bin_factor-1.0));
  }
 
  // deallocate memory
  fclose(F);
  free(delta_m);
  free(p_box); free(bin_ave); free(in_bin_ct);
  return 0;
}

int Tocmfast::moments(float *box, int boxtype, double &sig2, double &skew, double &kurt, double &ave)
{
  /*
    Returns the main central moments, i.e. variance, skew, kurt and the average
   */
  int pix_ct(0), total_pix(0);
  double nu;

  // Calculate the average and set the protected class variable accordingly
  ave = averagebox(box, boxtype, 0);
  setave(ave);
  if (DEBUG<=1) fprintf(DBUG,"\n In Tocmfast::moments\n average of box =  %f\n",ave);

  //adjust the number of dT pixels if we're modelling the instrumentals.
  if ( (model_inst==1) && (boxtype==2) )  
    total_pix=getNEW_TOT_NUM_PIXELS();  
  else 
    total_pix=HII_TOT_NUM_PIXELS; 
  if (DEBUG<=1) fprintf(DBUG, "\n In Tocmfast::moments total pixels= %i\n",total_pix);

  for (int i=0;i<total_pix;i++) {
    nu = box[i]- ave;
    sig2 += pow(nu , 2.0);
    skew += pow(nu , 3.0);
    kurt += pow(nu , 4.0);
    pix_ct++;
  }

  if ( !(pix_ct==total_pix) ) {
      fprintf(stderr, "\n !!! ERROR... Aborting Tocmfast::moments !!! \n the true total of pixels and the number that have contributed to the moment cacluations do not add up: true = %i, looped over = % i \n", total_pix, pix_ct);
      return 0;
  }
  sig2 /= pix_ct;
  skew /= pix_ct;
  kurt /= pix_ct;
  
  return 1;
  
}

double Tocmfast::nth_moment(float *box, int boxtype, int nonZero, int n)
{

  /* calculates the n^{th} central moment of *box of type boxtype
     nonZero can also be used to calculate the moment for non-zero 
     pixels only. This was superceded by moments function as more efficient
   */
  double moment(0.0), ave(0.0);
  int pixels(0),pixels_non0(0), total_pix(0);
  double threshold(0.015);

  ave = averagebox(box, boxtype, nonZero);


  setave(ave);
  if (DEBUG<=1) fprintf(DBUG,"\n In Tocmfast::nth_moment\n n = %i &  average of box =  %f\n",n,ave);

  if ((model_inst==1) && (boxtype==2))  //adjust the number of dT pixels if we're modelling the instrumentals.
    total_pix=getNEW_TOT_NUM_PIXELS();  
  else 
    total_pix=HII_TOT_NUM_PIXELS; 
<<<<<<< HEAD
  if (DEBUG<=1) fprintf(DBUG, "\n In Tocmfast::nth_moment total pixels= %i\n",total_pix);
  //cout<<"total pixels in moments"<<total_pix<<endl;
=======
  if (DEBUG<=1) fprintf(DBUG, "\n In Tocmfast::moments total pixels= %i\n",total_pix);
>>>>>>> refs/remotes/origin/master

  for (int i=0;i<total_pix;i++) {
    if ( (nonZero==1) && (boxtype==2) ) {
      if (box[i]>threshold) {
	moment += pow( (box[i]- ave),n );
	pixels_non0++;
      }
    }
    else moment += pow( (box[i]- ave),n );
    pixels++;
    //cout<<n<<"\t"<<moment<<endl;
  }
  cout<<pixels<<"\t"<<pixels_non0<<endl;
  if ( (nonZero==1) && (boxtype==2) ) //moment of non-zero distribution of dT
    moment/=((double)pixels_non0); 
  else  //moment of full distribution
    moment /= ((double)pixels);
  return moment;
}

int Tocmfast::subsample(float *sample, int boxtype, int x, int y, int z,float *box)
{
  //takes original *box from simulation and returns the sub-sample that corresponds with the x, y, z co-ordinate of the sub-sample set of boxes. The dimensions of x, y, z are controlled by NEW_DIM in TOCMFAST.h

  int istart(0), jstart(0), kstart(0), ct(0);
  
  istart = x*NEW_DIM;
  jstart = y*NEW_DIM;
  kstart = z*NEW_DIM;

  if (DEBUG>0) {
    fprintf(DBUG, "new dimension in subsample %i\n", NEW_DIM);
    fprintf(stderr,"x, y, z = {%i, %i, %i}\n",x,y,z);
    fprintf(stderr, "istart, jstart, kstart = {%i, %i, %i}", istart,jstart,kstart);
    fprintf(stderr, "istop,jstop,kstop={%i, %i, %i}",istart+NEW_DIM, jstart+NEW_DIM,kstart+NEW_DIM);
  }


  ct=0;
  //cout<<"c start value = "<<ct<<endl;
  for (int i=istart;i<(istart+NEW_DIM);i++) {
    for (int j=jstart;j<(jstart+NEW_DIM);j++) {
      for (int k=kstart;k<(kstart+NEW_DIM);k++) {
	if (model_inst==1)
<<<<<<< HEAD
	  sample[ct]=box[(k)+getDIMstore()*((j)+getDIMstore()*(i))];
=======
	  sample[ct]=box[(k) + getDIMstore()*( (j) + getDIMstore()*(i) )];
>>>>>>> refs/remotes/origin/master
	else
	  sample[ct]=box[HII_R_INDEX(i,j,k)];

	ct+=1;
      }
    }
  }

  //cout<<"c end value = "<<ct<<endl;
  return 0;
}

/***********************************************/
/*************** BOOTSTRAP STATS ***************/
/***********************************************/

void Tocmfast::bootstrap(float *box, int boxtype, double *mean_stat, double *std_stat)
{
  unsigned int i,j, Index;
  float _index;
  float *boxStar;
  double S2[BOOTSAMPLE], S3[BOOTSAMPLE];
  long RNG2SEED(-281377), RNG1SEED(-593967827); //random # gen seeds

  /* Takes an input *box and resamples it BOOTSTRAP (in TOCMFAST.h) times
with replacement. 

Calculates variance of the sample sig2, skewness and the
skew/sig2 std_stat index 0,1,2 respectively. In doing so also calculates 
the mean of each statistic mean_stat index 0,1,2 respectively. */
  boxStar = (float *) malloc(sizeof(float)*HII_TOT_NUM_PIXELS);

  for (i=0;i<BOOTSAMPLE;i++) {
    //cout<<"first loop, i="<<i<<endl;
    //Loop to create the ith sample
    for (j=0; j<HII_TOT_NUM_PIXELS; j++){
      _index = dran2(&RNG2SEED)*HII_TOT_NUM_PIXELS;
      // Decide whether to round up or down
      if ( _index - (floor(_index)) < 0.5 ) {
	Index = floor(_index);
      }
      else {
	Index = ceil(_index);
      }
      if (Index>HII_TOT_NUM_PIXELS) Index=HII_TOT_NUM_PIXELS;
      boxStar[j] = box[Index];
    }

    //calculate the moments for ith box sample
    S2[i]=nth_moment(boxStar, boxtype, 0, 2);
    S3[i]=nth_moment(boxStar, boxtype, 0, 3);

    // sum for the mean of moment statistics in which we are primarily concerned (can easily adapt or exapnd this for different choices of statistic)
    mean_stat[0]+= S2[i];
    mean_stat[1]+= S3[i]/pow(S2[i],1.5); //gamma_3
    mean_stat[2]+= S3[i]/S2[i]; //gamma_3'
 
    // Create new seed to get a fresh random sequence from rand2
    RNG2SEED = ran1(&RNG1SEED)*(-1*RNG2SEED);
    //cout<<RNG2SEED<<endl;
  }
  // Calculate the mean for our statistics
  for (i=0;i<3;i++) {
    //cout<<"second loop, i="<<i<<endl;
    mean_stat[i]/=BOOTSAMPLE;
  }

  // Calculate the variance for each stat:    
  for (i=0;i<BOOTSAMPLE;i++) {
    //cout<<"third loop, i="<<i<<endl;
    std_stat[0]+=pow( (S2[i]-mean_stat[0]),2);
    std_stat[1]+=pow( (S3[i]/pow( S2[i],1.5 ) - mean_stat[1]),2);
    std_stat[2]+=pow( (S3[i]/S2[i] - mean_stat[2]),2 );
  }
  for (i=0;i<3;i++) {
    //cout<<"fourth loop, i="<<i<<endl;
    std_stat[i]=sqrt(std_stat[i]/BOOTSAMPLE);
  }
  free(boxStar);
} // bootstrap end

/***************************************************
       HELPER ROUTINES
***************************************************/
float Tocmfast::noise_Sig()
{
  //returns the variance of the error on a pixel in mK due to instrumental noise
  float Sig;
  double ang_res, com_dist, pixel_size,SpecRes, H0;
  
  Sig=0.0;

  pixel_size = (double)BOX_LEN/(double)NEW_DIM;
  //cout<<"pix (Mpc)="<<pixel_size<<endl;
  H0 = 100.0*c->getH();
  com_dist = c->relativeConformalR(0, getz());
  //cout<<"comoving distance @ z="<<getz()<<" = "<<com_dist<<endl;
  // cout<<"sqrt(Om_m)="<<c->getOmega0()<<"H0="<<H0<<endl;
  ang_res = pixel_size /(PI/10800.0) /com_dist; //angular resolution in arcmins
  //cout<<"angular resolution= "<<ang_res<<endl;
  SpecRes = pixel_size*H0*nu_0*sqrt(c->getOmega0())/(SPEEDOFLIGHT_KMS*sqrt(1.0+getz()));
  //cout<<"spectral resolution= "<<SpecRes<<endl;
  Sig=2.9*(1.0e5/Atot)*pow( (10.0/ang_res),2 )*pow( ((1.0+getz())/10.0),4.6 )*pow( (1.0/SpecRes*100.0/int_time),0.5 );  
  //cout<<getz()<<"\t"<<Atot<<"\t"<<int_time<<endl;
  //cout<<Sig<<endl;

  return Sig;
}

void Tocmfast::HII_filter(fftwf_complex *box, int filter_type, float R){
  int n_x, n_z, n_y;
  float k_x, k_y, k_z, k_mag, kR;

  // loop through k-box
  for (n_x=0; n_x<HII_DIM; n_x++){
    if (n_x>HII_MIDDLE) {k_x =(n_x-HII_DIM) * DELTA_K;}
    else {k_x = n_x * DELTA_K;}

    for (n_y=0; n_y<HII_DIM; n_y++){
      if (n_y>HII_MIDDLE) {k_y =(n_y-HII_DIM) * DELTA_K;}
      else {k_y = n_y * DELTA_K;}

      for (n_z=0; n_z<=HII_MIDDLE; n_z++){ 
	k_z = n_z * DELTA_K;
	
	k_mag = sqrt(k_x*k_x + k_y*k_y + k_z*k_z);

	kR = k_mag*R; // real space top-hat
	if (filter_type == 0){ // real space top-hat
	  if (kR > 1e-4){
	    box[HII_C_INDEX(n_x, n_y, n_z)] *= 3.0 * (sin(kR)/pow(kR, 3) - cos(kR)/pow(kR, 2));
	  }
	}
	else if (filter_type == 1){ // k-space top hat
	  kR *= 0.413566994; // equates integrated volume to the real space top-hat (9pi/2)^(-1/3)
	  if (kR > 1){
	    box[HII_C_INDEX(n_x, n_y, n_z)] = 0;
	  }
	}
	else if (filter_type == 2){ // gaussian
	  kR *= 0.643; // equates integrated volume to the real space top-hat
	  box[HII_C_INDEX(n_x, n_y, n_z)] *= pow(E, -kR*kR/2.0);
	}
	else{
	  fprintf(stderr, "\n In Tocmfast::HII_filter \n Warning, filter type %i is undefined\nBox is unfiltered\n", filter_type);
	  return;
	}
      }
    }
  } // end looping through k box
  

}

void Tocmfast::NEW_filter(fftwf_complex *box, int filter_type, float R){
  int n_x, n_z, n_y;
  float k_x, k_y, k_z, k_mag, kR;

  // loop through k-box
  for (n_x=0; n_x<NEW_DIM; n_x++){
    if (n_x>NEW_MIDDLE) {k_x =(n_x-NEW_DIM) * DELTA_K;}
    else {k_x = n_x * DELTA_K;}

    for (n_y=0; n_y<NEW_DIM; n_y++){
      if (n_y>NEW_MIDDLE) {k_y =(n_y-NEW_DIM) * DELTA_K;}
      else {k_y = n_y * DELTA_K;}

      for (n_z=0; n_z<=NEW_MIDDLE; n_z++){ 
	k_z = n_z * DELTA_K;
	
	k_mag = sqrt(k_x*k_x + k_y*k_y + k_z*k_z);

	kR = k_mag*R; // real space top-hat
	if (filter_type == 0){ // real space top-hat
	  if (kR > 1e-4){
	    box[NEW_C_INDEX(n_x, n_y, n_z)] *= 3.0 * (sin(kR)/pow(kR, 3) - cos(kR)/pow(kR, 2));
	  }
	}
	else if (filter_type == 1){ // k-space top hat
	  kR *= 0.413566994; // equates integrated volume to the real space top-hat (9pi/2)^(-1/3)
	  if (kR > 1){
	    box[NEW_C_INDEX(n_x, n_y, n_z)] = 0;
	  }
	}
	else if (filter_type == 2){ // gaussian
	  kR *= 0.643; // equates integrated volume to the real space top-hat
	  box[NEW_C_INDEX(n_x, n_y, n_z)] *= pow(E, -kR*kR/2.0);
	}
	else{
	  fprintf(stderr, "\n In Tocmfast::HII_filter \n Warning, filter type %i is undefined\nBox is unfiltered\n", filter_type);
	  return;
	}
      }
    }
  } // end looping through k box
  

}

double Tocmfast::averagebox(float *box, int boxtype, int nonZero) {

  //Finds the average of real *box

  double ave(0.0);
  long long int count(0),count_non0(0), total_pix(0);
  float threshold(0.015);

  if ( (model_inst==1) && (boxtype==2) )  //adjust the number of pixels if we're modelling the instrumentals. 
    total_pix=getNEW_TOT_NUM_PIXELS();  
  else 
    total_pix=HII_TOT_NUM_PIXELS;

  for (int i=0; i<total_pix; i++){
    // If we are calculating the average of the non-zero distribution and the box value is above the threshold. Should only happen for dT boxes:
    if ( (nonZero==1)&&(boxtype==2) ) { 
      if (box[i]>threshold) {
	//cout<<"non zero skew\n";
	ave += box[i];
	count_non0+=1;
      }
    }
    // If we are calculating the average of the full distribution:
    else {
	  ave += box[i];
    }	 
    //cout<<box[i]<<endl;
    count+=1;
  }

  if ((nonZero==1) && (boxtype==2) ) count=count_non0;

  ave /= ((double)(count));

  if (DEBUG>=4) fprintf(stderr, "\nIn Tocmfast::averagebox \n average = %f, boxtype = %i, count = %i\n",ave, boxtype, int(count));
  return ave;
} //end averagebox



const char* Tocmfast::boxstring(int boxtype) {
  
  const char *type;

  switch (boxtype) {
  case 1:
    type = "xH";
    break;
  case 2:
    type = "dT";
    break;
  case 3:
    type="density";
    break;
  default:
    fprintf(stderr,"Tocmfast::boxstring -> You need to choose a boxtype in your call or power spectrum: 1 = xH, 2= dT, 3=density");
    type="null";
  }
  return type;
}


// helper function for update in sphere below
void Tocmfast::check_region(float * box, int dimensions, float Rsq_curr_index, int x, int y, int z, int x_min, int x_max, int y_min, int y_max, int z_min, int z_max){
  int x_curr, y_curr, z_curr, x_index, y_index, z_index;
  float xsq, xplussq, xminsq, ysq, yplussq, yminsq, zsq, zplussq, zminsq;

  for (x_curr=x_min; x_curr<=x_max; x_curr++){
    for (y_curr=y_min; y_curr<=y_max; y_curr++){
      for (z_curr=z_min; z_curr<=z_max; z_curr++){
	x_index = x_curr;
	y_index = y_curr;
	z_index = z_curr;
	// adjust if we are outside of the box
	if (x_index<0) {x_index += dimensions;}
	else if (x_index>=dimensions) {x_index -= dimensions;}
	if (y_index<0) {y_index += dimensions;}
	else if (y_index>=dimensions) {y_index -= dimensions;}
	if (z_index<0) {z_index += dimensions;}
	else if (z_index>=dimensions) {z_index -= dimensions;}

	// now check
	//	printf("checking %i, %i, %i\n", x_index, y_index, z_index);
	//printf("this is index, %llu\n", HII_R_INDEX(x_index, y_index, z_index));
	//fflush(NULL);
	if (box[HII_R_INDEX(x_index, y_index, z_index)]){ // untaken pixel (not part of other halo)
	  //	  printf("in if\n");
	  // fflush(NULL);
	  // remember to check all reflections
	  xsq = pow(x-x_index, 2);
	  ysq = pow(y-y_index, 2);
	  zsq = pow(z-z_index, 2);
	  xplussq = pow(x-x_index+dimensions, 2);
	  yplussq = pow(y-y_index+dimensions, 2);
	  zplussq = pow(z-z_index+dimensions, 2);
	  xminsq = pow(x-x_index-dimensions, 2);
	  yminsq = pow(y-y_index-dimensions, 2);
	  zminsq = pow(z-z_index-dimensions, 2);
	  if ( (Rsq_curr_index > (xsq + ysq + zsq)) || 
	       (Rsq_curr_index > (xsq + ysq + zplussq)) || 
	       (Rsq_curr_index > (xsq + ysq + zminsq)) || 

	       (Rsq_curr_index > (xsq + yplussq + zsq)) || 
	       (Rsq_curr_index > (xsq + yplussq + zplussq)) || 
	       (Rsq_curr_index > (xsq + yplussq + zminsq)) || 

	       (Rsq_curr_index > (xsq + yminsq + zsq)) || 
	       (Rsq_curr_index > (xsq + yminsq + zplussq)) || 
	       (Rsq_curr_index > (xsq + yminsq + zminsq)) || 


	       (Rsq_curr_index > (xplussq + ysq + zsq)) || 
	       (Rsq_curr_index > (xplussq + ysq + zplussq)) || 
	       (Rsq_curr_index > (xplussq + ysq + zminsq)) || 

	       (Rsq_curr_index > (xplussq + yplussq + zsq)) || 
	       (Rsq_curr_index > (xplussq + yplussq + zplussq)) || 
	       (Rsq_curr_index > (xplussq + yplussq + zminsq)) || 

	       (Rsq_curr_index > (xplussq + yminsq + zsq)) || 
	       (Rsq_curr_index > (xplussq + yminsq + zplussq)) || 
	       (Rsq_curr_index > (xplussq + yminsq + zminsq)) || 


	       (Rsq_curr_index > (xminsq + ysq + zsq)) || 
	       (Rsq_curr_index > (xminsq + ysq + zplussq)) || 
	       (Rsq_curr_index > (xminsq + ysq + zminsq)) || 

	       (Rsq_curr_index > (xminsq + yplussq + zsq)) || 
	       (Rsq_curr_index > (xminsq + yplussq + zplussq)) || 
	       (Rsq_curr_index > (xminsq + yplussq + zminsq)) || 

	       (Rsq_curr_index > (xminsq + yminsq + zsq)) || 
	       (Rsq_curr_index > (xminsq + yminsq + zplussq)) || 
	       (Rsq_curr_index > (xminsq + yminsq + zminsq))
	     ){
	    
	    // we are within the sphere defined by R, so change flag in box array
	    	    box[HII_R_INDEX(x_index, y_index, z_index)] = 0;
	    //	    box[HII_R_INDEX(x_index, y_index, z_index)] = 15;
	    //printf("%i, %i, %i\n", x_index, y_index, z_index);
		    //		    fflush(NULL);
	  }
	}
      }
    }
  }
}


/*
  Function UPDATE_IN_SPHERE takes in a <box> and flags all points
  which fall within radius R of (x,y,z).
  all lengths are in units of box size.
*/
void Tocmfast::update_in_sphere(float * box, int dimensions, float R, float xf, float yf, float zf, int InvMHR){
  int x_curr, y_curr, z_curr, xb_min, xb_max, yb_min, yb_max, zb_min, zb_max, R_index;
  int xl_min, xl_max, yl_min, yl_max, zl_min, zl_max;
  float Rsq_curr_index;
  int x_index, y_index, z_index, x, y, z;

  if (R<0) return;
  //  printf("in update, dim=%i, R=%f, x=%f, y=%f, z=%f\n", dimensions, R, xf, yf, zf);
  //fflush(NULL);
  // convert distances to index units
  x = (int) (xf * dimensions + 0.5); // +0.5 for rounding
  y = (int) (yf * dimensions + 0.5);
  z = (int) (zf * dimensions + 0.5);


  /*****  first, just automatically fill in the inner cube whose diagonal is R, side is 2R/sqrt(3) *****/
  R_index = ceil(R/sqrt(3.0)*dimensions)-1;
  // set parameter range
  xl_min = x-R_index;
  xl_max = x+R_index;
  yl_min = y-R_index;
  yl_max = y+R_index;
  zl_min = z-R_index;
  zl_max = z+R_index;

  for (x_curr=xl_min; x_curr<=xl_max; x_curr++){
    for (y_curr=yl_min; y_curr<=yl_max; y_curr++){
      for (z_curr=zl_min; z_curr<=zl_max; z_curr++){
	x_index = x_curr;
	y_index = y_curr;
	z_index = z_curr;
	// adjust if we are outside of the box
	if (x_index<0) {x_index += dimensions;}
	else if (x_index>=dimensions) {x_index -= dimensions;}
	if (y_index<0) {y_index += dimensions;}
	else if (y_index>=dimensions) {y_index -= dimensions;}
	if (z_index<0) {z_index += dimensions;}
	else if (z_index>=dimensions) {z_index -= dimensions;}

	// now just paint it
	//box[HII_R_INDEX(x_index, y_index, z_index)] = 15;
	if (InvMHR==1) 
	  box[HII_R_INDEX(x_index, y_index, z_index)] = 0;
	else
	  box[HII_R_INDEX(x_index, y_index, z_index)] = 1;
      }
    }
  }


  /****** now check the pixels between the smaller and the larger cube which encloses the sphere ******/
  R_index = ceil(R*dimensions);
  Rsq_curr_index = pow(R*dimensions, 2); // convert to index
  // set parameter range
  xb_min = x-R_index;
  xb_max = x+R_index;
  yb_min = y-R_index;
  yb_max = y+R_index;
  zb_min = z-R_index;
  zb_max = z+R_index;

  //    check_region(box, dimensions, Rsq_curr_index, x,y,z, xb_min, xb_max, yb_min, yb_max, zb_min, zb_max);
  
  check_region(box, dimensions, Rsq_curr_index, x,y,z, xb_min, xl_min, yb_min, yb_max, zb_min, zb_max);
  check_region(box, dimensions, Rsq_curr_index, x,y,z, xl_max, xb_max, yb_min, yb_max, zb_min, zb_max);

  check_region(box, dimensions, Rsq_curr_index, x,y,z, xb_min, xb_max, yb_min, yl_min, zb_min, zb_max);
  check_region(box, dimensions, Rsq_curr_index, x,y,z, xb_min, xb_max, yl_max, yb_max, zb_min, zb_max);

  check_region(box, dimensions, Rsq_curr_index, x,y,z, xb_min, xb_max, yb_min, yb_max, zb_min, zl_min);
  check_region(box, dimensions, Rsq_curr_index, x,y,z, xb_min, xb_max, yb_min, yb_max, zl_max, zb_max);
}


/*********************************************************************
 FOR UPDATING/ACCESSING VALUES OF PROTECTED MEMBER VARIABLES 
*********************************************************************/
// UPDATE:-
void Tocmfast::setz(double z)
{
  z1=z; // Updates the redshift
}
void Tocmfast::setnf(double nf)
{
  nf1=nf; // Updates the redshift
}
void Tocmfast::setR(double R)
{
  R1=R;
}
void Tocmfast::setZeta(double Zeta)
{
  Zeta1=Zeta;
}
void Tocmfast::setave(double avg)
{
  avg1=avg;
}
void Tocmfast::setSig2(double sigma2)
{
  SiGma21 = sigma2;
}
void Tocmfast::setNewDim(int NewDim)
{
  NEW_DIM = NewDim;
}
void Tocmfast::setDIMstore(int DimStore)
{
  DIM_STORE = DimStore;
}

// ACCESS:-
double Tocmfast::getz()
{
  return z1;
}
double Tocmfast::getnf()
{
  return nf1;
}
double Tocmfast::getR()
{
  return R1;
}
double Tocmfast::getZeta()
{
  return Zeta1;
}
double Tocmfast::getave()
{
  return avg1;
}
double Tocmfast::getSig2()
{
  return SiGma21;
}
/*int Tocmfast::getNEW_R_INDEX(int x,int y,int z)
{  
  return (z)+NEW_DIM*((y)+NEW_DIM*(x));
  } Replaced by Real1Dindex*/
int Tocmfast::getNEW_TOT_NUM_PIXELS()
{
  return NEW_DIM*NEW_DIM*NEW_DIM;
}
int Tocmfast::getDIMstore()
{
  return DIM_STORE;
}

// NON-CLASS restricted routines
int import_as_fft(char *file, int res, int boxtype, fftwf_complex *box_fft) {

  fftwf_plan plan;
  float *realbox;
  FILE *F;
  
  realbox = (float *)malloc(sizeof(float)*HII_TOT_NUM_PIXELS);

  // Read in data from file
  F = fopen(file, "rb");
  if ( (boxtype==3) && !(SmoothedDensityBox==1) ) { //read in for a padded density box   
    fprintf(stderr, "tocmfast >> import_as_fft \n Reading in FFT padded density box\n");
    if (mod_fread(box_fft, sizeof(fftwf_complex)*HII_KSPACE_NUM_PIXELS, 1, F)!=1){
      fftwf_free(box_fft); fclose(F);
      fprintf(stderr, " tocmfast >> import_as_fft \n unable to read-in file\nAborting\n");
      return -1;
    }
  }
  else {  
    //fprintf(stderr, "Reading in realbox of HII_DIM=%i\n & pass it into FFT padded box\n", HII_DIM);
    for (int i=0; i<res; i++){
      for (int j=0; j<res; j++){
	for (int k=0; k<res; k++){
	  if (fread(&realbox[HII_R_INDEX(i,j,k)], sizeof(float), 1, F)!=1){
	    fprintf(stderr, "tocmfast >> import_as_fft \n Read error occured while reading box in passed argv.\n");
	    fclose(F); free(realbox);
	    return -1;
	  }
	  *((float *)box_fft + HII_R_FFT_INDEX(i,j,k)) = (realbox[HII_R_INDEX(i,j,k)]); // Following the standards of D Jeong 2010 (dissertation) so deal with DFT normalisations in the definition of the bispectrum estimator
	}
      }
    }
    fclose(F);
  }
  
  //If you're playing with a density box then deal with the question of converting back from DELTA or not (DELTA=rho/<rho> instead of rho/<rho>-1) and check its physicality
  if (boxtype==3) {
    for (int i=0; i<res; i++){
      for (int j=0; j<res; j++){
	for (int k=0; k<res; k++){
	  if (DELTA==1) {
	    *((float *)box_fft + HII_R_FFT_INDEX(i,j,k)) -= 1;
	    //fprintf(stderr, "\nTocmfast::importbox:: Converting padded DELTA box to padded delta box\n");
	    if (*((float *)box_fft + HII_R_FFT_INDEX(i,j,k)) < -1.0){
	      fprintf(stderr, "tocmfast >> import_as_fft \n DELTA less than 0???\n");
	    }
	  }
	  else {
	    if (*((float *)box_fft + HII_R_FFT_INDEX(i,j,k)) < -1.0){
	      fprintf(stderr, "tocmfast >> import_as_fft \n delta less than -1???\n");
	    }
	  }

	}
      }
    } //END loops over ijk 
  }
// transform to k-space
  plan = fftwf_plan_dft_r2c_3d(res, res, res, (float *)box_fft, (fftwf_complex *)box_fft, FFTW_ESTIMATE);
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);
  fftwf_cleanup();

  return 1;
}

/*********************************************************************
 Wrapper functions for the std library functions fwrite and fread
 which should improve stability on certain 64-bit operating systems
 when dealing with large (>4GB) files - LIFTED FROM 21CMFAST
*********************************************************************/
size_t mod_fwrite (const void *array, unsigned long long size, unsigned long long count, FILE *stream){
  unsigned long long pos, tot_size, pos_ct;
  const unsigned long long block_size = 4*512*512*512;

  tot_size = size*count; // total size of buffer to be written

  //check if the buffer is smaller than our pre-defined block size
  if (tot_size <= block_size)
    return fwrite(array, size, count, stream);
  else{
    pos = 0;
    pos_ct=0;
    // buffer is large, so write out in increments of block_size
    while ( (pos+block_size) < tot_size ){
      fwrite((char *)array+pos, block_size, 1, stream);
      pos_ct++;
      pos = block_size*pos_ct;
    }
    return fwrite((char *)array+pos, tot_size-pos, 1, stream);
  }
}

size_t mod_fread(void * array, unsigned long long size, unsigned long long count, FILE * stream){
  unsigned long long pos, tot_size, pos_ct;
  const unsigned long long block_size = 512*512*512*4;

  tot_size = size*count; // total size of buffer to be written                                                         
  //check if the buffer is smaller than our pre-defined block size
  if (tot_size <= block_size)
    return fread(array, size, count, stream);
  else{
    pos = 0;
    pos_ct=0;
    // buffer is large, so write out in increments of block_size        
    while ( (pos+block_size) < tot_size ){
      fread((char *)array + pos, block_size, 1, stream);
      pos_ct++;
      pos = block_size*pos_ct;
    }
    return fread((char *)array+pos, tot_size-pos, 1, stream);
  }
}


/*
  // SKEW FILE OUTPUT 
  type=boxstring(boxtype);
  sprintf(filename, "mkdir Output_files/SkewFiles/");
  system(filename);
  sprintf(outputdir, "Output_files/SkewFiles/%s/",type);
  sprintf(filename, "mkdir %s", outputdir);
  system(filename);

  sprintf(filename, "%s%sHist_z%.2f_smoothingR%.2f_nf%f_%i_%.0fMpc",outputdir,type,getz(),getR(), getnf(), HII_DIM, BOX_LEN);
  F = fopen(filename, "w");
  if (!F){
    fprintf(stderr, "Tocmfast::skew3Dbox-> Unable to open file: %s for writing\nCheck that the variXtract routines have been called \nAborting...\n", filename);
    free(box);
    return -1;
  }

  fprintf(, "%.2f \t %f \t %f \n",getz(), skew, getnf());*/



/****************** UNFINISHED ROUTINES: *********************/



double Tocmfast::moments_w_err(float *box, int boxtype, int nonZero, double &kurt_Sig2, double &skew_Sig2, double &skew_norm_Sig2, double &Sig2_Sig2, double &kurt_avg, double &skew_avg, double &skew_norm_avg, double &Sig2_avg)
{
  //calulate the skew & error by sampling over the clean box with random samples of noise PDF
  //NOTE THIS IS ONLY TO BE USED WHEN THE BOX HAS BEEN RESAMPLED
  int sample_count(1000);
  long long int count(0),ct(0);
  long RNG2SEED(-28195747), RNG1SEED(-349675245); //random # gen seeds
  float *noisy_box;
  double *skew, *skew_norm, *Sig2,*kurt, *mean, mean_avg(0);

  // allocate memory to pointer arrays
  noisy_box = (float *)malloc(sizeof(float)*NEW_TOT_NUM_PIXELS); //box for signal + noise random samples
  skew = (double *)malloc(sizeof(double)*sample_count); 
  kurt = (double *)malloc(sizeof(double)*sample_count); 
  mean = (double *)malloc(sizeof(double)*sample_count);
  Sig2 = (double *)malloc(sizeof(double)*sample_count);

  for (int i=0; i<sample_count; i++) { //loop to generate skew samples
    ct=0;
    //loop to generate a noisy box & calulate the average sum
    for (int x=0; x<NEW_DIM; x++){
      for (int y=0; y<NEW_DIM; y++){
	for (int z=0; z<NEW_DIM; z++){	  	 

	  noisy_box[NEW_R_INDEX(x,y,z)]=(gasdev(&RNG1SEED)*noise_Sig());//box[NEW_R_INDEX(x,y,z)]+(gasdev(&RNG1SEED)*noise_Sig());
	  //cout<<gasdev(&RNG1SEED)<<"\t"<<noise_Sig()<<endl;
	  mean[i]+=noisy_box[NEW_R_INDEX(x,y,z)];
	  ct++;
	} //end z loop
      } //end y loop
    } //end x loop

    if (!(ct==NEW_TOT_NUM_PIXELS)) {
      fprintf(stderr, "\n In Tocmfast::skew_w_err -> we are missing pixels in our loop to create the noisy box \nAborting...\n");
      free(box); free(noisy_box);
      return -1; 
    }
    
    skew[i]=nth_moment(noisy_box, 2, nonZero, 3);
    Sig2[i]=nth_moment(noisy_box, 2, nonZero, 2);//-pow(noise_Sig(),2);
    kurt[i]=nth_moment(noisy_box, 2, nonZero, 4);
    mean[i]/=ct;

    // Calculate skew average summation
    skew_avg+=skew[i];
    kurt_avg+=kurt[i];
    Sig2_avg+=Sig2[i];
    mean_avg+=mean[i];

    //Make new seed for next random number sequence
    RNG1SEED = ran1(&RNG2SEED)*(-1*RNG1SEED);
    count++;
    /*    for (int x=0; x<NEW_DIM; x++){
      for (int y=0; y<NEW_DIM; y++){
	for (int z=0; z<NEW_DIM; z++){
	  noisy_box[NEW_R_INDEX(x,y,z)]=0.0;
	}
      }
      }*/
  } // end main i loop
  cout<<"sample count ="<<count<<"& pixels in box = "<<ct<<endl;
  skew_avg/=((double)(count));
  kurt_avg/=((double)(count));
  Sig2_avg/=((double)(count));
  mean_avg/=((double)(count));
  cout<<" normalised skew="<<pow(Sig2_avg,1.5)<<"\t skew_evg="<<skew_avg<<"\t Sig2_evg="<<Sig2_avg<<endl;
  skew_norm_avg=skew_avg/pow(Sig2_avg,1.5);
  cout<<"skew, normalised skew & Sigma^2 averages: "<<skew_avg<<"\t"<<skew_norm_avg<<"\t"<<Sig2_avg<<endl;

  count=0;
  for (int j=0;j<sample_count; j++) { //loop to calculate variance of our skew
    skew_Sig2 += pow((skew[j]-skew_avg),2.0);
    kurt_Sig2 += pow((kurt[j]-kurt_avg),2.0);
    Sig2_Sig2 += pow((Sig2[j]-Sig2_avg),2.0);
    count++;
  } // end k loop
  cout<<"sample count = "<<count<<endl;
  cout<<skew_Sig2<<endl;

  skew_Sig2/=((double)(count));
  kurt_Sig2/=((double)(count));
  Sig2_Sig2/=((double)(count));
  skew_norm_Sig2=skew_Sig2/pow(Sig2_avg,1.5);

  //exportBox(noisy_box, getnf(), getz(), boxtype);
  cout<<"error on skew: "<<sqrt(skew_norm_Sig2)<<endl;
  return mean_avg;
  free(noisy_box);free(skew);free(skew_norm);free(Sig2);
}
