/* reionistat.cc
 *
 * Includes utility functions for reionisation connected statistics. CAW2012.
 * Nb. Uses many dcosmology.cc routines
 * 
 */

// std headers:
#include <math.h>
#include <iostream> 
#include <fstream> 
#include <cstdlib>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>

// Bespoke headers:
#include "dcosmology.h"
#include "../USERFLAGS.h"
#include "REIONISE.h"
#include "ANAL_PARAMS.H"
#include "INIT_PARAMS.H"

using namespace std;

/* REFERENCES:
 * Furlanetto, Zaldarriaga & Hernquist 2004 (hereafter FZH04)
 * 21CMFAST PAPER
 * Watkinson & Pritchard 2014 (arXiv:1312.1342)
 */

/* DEVELOPMENT NOTES
 * Check that update_in_sphere has been adapted to allow proper operation of
 * Global MHR	
 */

/**********************************************************************
 ********************* Constructor/Destructor *************************
 *********************************************************************/

Reionise::Reionise(Cosmology *c1,  Tocmfast *T01, double z, FILE *LOG)
{
  setz(z);
  C=c1;
  T0=T01;
  DBUG = LOG;
  PsiUnobtain=-10.0; //this variable is used in FZH to abort calculating the PDF for values greater than a value of Psi for which the program has already established is unobtainable. Nb. psi always positive in the FZH model
  DELTAION=-10.0; //Used in MHR model as the density threshold for ionisation
  // Set the default behavious to be normal FZH04 (i.e. Global-outside-in) and standard press-schechter
  setK(inverf(1.0-pow(ZETA,-1.0)));
  setBtype(0);
}

Reionise::~Reionise()
{
  //  cout << "Destructor called for Cosmology" << endl;
}


/*************************************************
           SOBACHI & MESINGER SHIZZM
*************************************************/
int Reionise::galacticHI(float *xH, float *delta, float alpha, int res, float L)
{
  //uses fcoll in a cell as a proxy for the fraction in galaxies. The fraction of galactic HI per galaxy is given by alpha

  //float galHI(0);
  int count(0), Npix(0);
  double Mpix, Vpix;

  if ((alpha<0.0)||(alpha>1.0)) {
    fprintf(stderr,"Reionise->galacticHI: 0<=alpha<=1, your choice is inappropriate\n ABORTING.....");
    return -1;
  }

  Vpix = pow((L+0.0/res),3);
  Npix=pow(res,3);

  for (int i=0; i<Npix; i++){
    //xH[i]=0; //cw temp 	
    //cout<<delta[i]<<"\t"<<xH[i]<<"\t"<<fcollExPS(delta[i])<<endl;
    Mpix = Vpix*( (delta[i]+1.0)*CRITDENMSOLMPC*C->getOm0hh() );
    setMpix(Mpix); //for call to getsigpix() in fcollExPS
    if (SobBox==1) //need to work in terms of mass weighted neutral fractions
      xH[i] += fcollExPS(delta[i])*alpha;
    else
      xH[i] += fcollExPS(delta[i])*alpha*(delta[i]+1.0)/DEL_gal; 

    if (xH[i]>1) {
      //fprintf(stderr, "adjusted neutral fraction = %.5f, setting to 1\n", xH[i]);
      xH[i]=1;
      }
    count+=1;
    //cout<<i<<endl;
    
  }
  if (!(count==HII_TOT_NUM_PIXELS)) {
    fprintf(stderr, "TOCM::galacticHI\n PROBLEM pixel count does not equal the total pixels expected in out input box: count=%i, HII_TOT_PIX = % i.... ABORTING.....",count , HII_TOT_NUM_PIXELS);
    return -1;
  }
  return 1;
}

/*********************************************************************
* Functions involved in calculating 21cm statistics from FZH04 model *
*********************************************************************/

// pdfPsi -> Full pdf for psi  
double Reionise::pdfPsi(double psi)
{
  double prob, psimx, deltamx, norm;
  //We need to make sure that the weight of the very small -> 0 pixel values are included, can be missed with the qromo algorithm as it interpolates at edges.
  if (psi<=0.01) {//if (psi==0.0) {
    if (gethard_code_gen()) {
      getPpsi0(prob); 
    }
    else {
      psimx = calcPsiMax(deltamx,200.0,-20.0,20.0);
      normPDF(norm,psimx);
      prob = 1.0-norm;
    }
  }
  else prob = P_psi(psi);

  if (DEBUG>=3) fprintf(DBUG,"\n For Psi = %.2f, the corresponding probability density = %e \t Reionise::pdfPsi \n",psi,prob);

  return prob;
}

// P_psi -> PDF of psi from FZH04 eqn 32
double Reionise::P_psi(double psi)//, double z)
{
  double delta(0.0), Ppsi(0.0); 
  double temp (0.0);
  vector<double> roots;

  // Find values of delta that corespond to psi
  roots=deltaFromPsi(psi);

  for(unsigned int i=0;i<roots.size();i++){
    delta=roots[i];
    temp = P_delta(delta)*fabs( 1.0/dPsidDelta(delta) )*( 1.0-P_ion(delta) );
    if (DEBUG>=3) fprintf(DBUG, "\n z = %.2f\tPsi = %.4f \t delta%i(x) = %.4f \t P(delta%i) = %e dDelta/dPsi = %e \t P(ion) = %.4f \t contributing  %e to P(psi) \t Reionise::P_psi \n", getz(), psi, i, delta, i,P_delta(delta), fabs( 1.0/dPsidDelta(delta) ), P_ion(delta), temp);
    Ppsi+=temp;
   }
  return Ppsi;
}

/* P_ion -> NEED PROBABILITY THAT A PIXEL IS IONISED from eqn29 FZH04,
   i.e. The probability that a pixel of given delta_0 is ionised*/
double Reionise::P_ion(double delta0)
{
  double Pion, sigpix2;
  double tol(1.0e-4);
  
  setdelta0(delta0);
  sigpix2 = pow(getsigpix(),2.0);
 
  setPcross(0.0,this,1); //set the static cosmology class variable within the dummy call. 
  Pion = qromo(dummyPcross,0.0,sigpix2, midpnt,tol); 
  if (Pion>1.0) Pion = 1.0;
  if (DEBUG>=3) fprintf(DBUG, "\n----- In P_ion(delta0)-----\n delta0 = %e sigma(pix)^2 = %6.4f \t P(ion) = %6.4f \t Reionise::P_ion\n", delta0, sigpix2, Pion);
  return Pion;
  }

/* P_crossB -> eqn 31 FZH04 
   Probability that the barrier for ionisation has 
   been crossed given that the pixel of interest has 
   density contrast of delta0*/
double Reionise::P_crossB(double sigmaM2)
{
  double Pcross, B, sigpix2, delta0;
  double top, bot;
 
  sigpix2 = pow(getsigpix(),2.0);
  delta0 = getdelta0();

  B = Barrier(sigmaM2);
  Pcross = B/sqrt(2.0*PI);
  Pcross *= sqrt(sigpix2)/( pow(sqrt(sigmaM2), 3.0)*sqrt(sigpix2 -  sigmaM2) ) ;
  top = pow( ((delta0*sigmaM2) - (B*sigpix2)),2.0);
  bot = 2.0*sigmaM2*sigpix2*(sigpix2 -  sigmaM2);
  Pcross *= exp(-1.0*(top/bot));

  if (Pcross<0.0) {
    fprintf(stderr, "ABORTING ......... \nWhy? Because the barrier is no longer well behaved and so the analytic approach of FZH04 does not apply to this regime. \n Run the neutral fraction section alone for the redshift you've choosen and you should see the lowest z for which the Universe is not fully Reionised already, as is the case for a redshift of %4.2f \n", getz() );
    exit(0);
  }
  return Pcross;
}

double setPcross(double sigmaM2, Reionise *R1, int iflag)
{
  static Reionise *r;
  
  if(iflag==1){
    r=R1;
    return 0.0;
  }

  return r->P_crossB(sigmaM2);
}

double dummyPcross(double sigmaM2)
{
  return setPcross(sigmaM2,NULL,0);
}

/* Barrier(double sigmaM2) - eqn 4-6 FZH04
   Linear approximation to the barrier for ionisation */
double Reionise::Barrier(double sigmaM2)
{
  /* Calculates the ionisation barrier of FZH2004, the barrier
   * by default is that of the original paper. Setting it
   * to 1 prior to calling the barrier functions inverts the
   * barrier to be global-outside-in (use setBtype(1) in driver)
  */
  double B, K, sigMmin, z;

  z = getz();
  sigMmin= sigmaMmin();
  K=getK(); 
  //cout<<z<<"\t"<<K<<endl;

  B = B0(K, sigMmin);
  if (getBtype()<1) {
    B +=( K/(sqrt(2.0)*sigMmin)*sigmaM2 ); //Eqn 6 FZH04
    //cout<<"returned B="<<B<<endl;    
  }
  else if (getBtype()==1) {
    B -=( K/(sqrt(2.0)*sigMmin)*sigmaM2 ); // Eqn 6 FZH04 bastardised to be outside-in
    //cout<<"B="<<B<<"@ redshift "<<z<<"for zeta="<<ZETA<<endl; 
  }

  return B;
}
double Reionise::B0(double K, double sigMmin) 
{
  double B(0);

  if (getBtype()<1) {
    B= (C->delCrit0(getz())/C->growthFac(getz())) - sqrt(2.0)*K*sigMmin; //Eqn 5 FZH04
    //cout<<B<<endl;
    //cout<<"z="<<getz()<<"\t K="<<K<<endl;
    //cout<<getBtype()<<endl;
  }
  else if (getBtype()==1) {
    B= getA()+(C->delCrit0(getz())/C->growthFac(getz())) - sqrt(2.0)*K*sigMmin; //Eqn 5 FZH04 bastardised to be outside-in
    cout<<"A used in B0: "<<getA()<<endl;
  }
  return B;
}

double Reionise::P_delta(double deltaM)
{
  double sigmapix, Pdelta;

  sigmapix = getsigpix();
  Pdelta = exp( (pow(deltaM,2.0))/(-2.0*pow(sigmapix,2.0)) );
  Pdelta /= (sqrt(2.0*PI)*sigmapix);
  if (DEBUG>=3) fprintf(DBUG, "\n----- In P_delta(deltaM)-----\n deltaM = %6.4f \t  P(delta) = %6.4f\t Reionise::P_delta \n",deltaM, Pdelta);

  return Pdelta;
}

/*************************
 *  FZH04 HELPER FUNCTIONS 
 ************************/
// normPDF->Forced normalisation of pdf by assuming the non-zero PDF of FZH04 is a well behaved PDF and therefore assigning any deficit from 1.0 to the probability of psi=zero. Note there are hard-coded values for this that can be obtained using loadHC_Psi0.
void Reionise::normPDF(double &norm, double IntMaxLim)
{
  double tol(1.0e-4);

  setP_Psi(0.0, this,1);
  norm = qromo(dummyP_Psi, 0.01, IntMaxLim, midpnt, tol);
  setP_psiZero(1.0-norm);
}
double dummyP_Psi(double psi)
{
  return setP_Psi(psi, NULL,0);
}
double setP_Psi(double psi, Reionise *R1, int iflag)
{
 static Reionise *r;
  
  if(iflag==1){
    r=R1;
    return 0.0;
  }

  return r->P_psi(psi);
}


//void Reionise::calcPsiMax(double &psimax, double &deltamax, double psi, double root1, double root2) 
double Reionise::calcPsiMax(double &deltamax, double psi, double root1, double root2)
{
   //get crude estimate of midpoint
  double psimax(0.0);
  double delta(0.0), psit;
  double dstep(0.01);
  int count(0);
  
  psimax=0.0;
  while(psimax<psi && count<2){
    psit=0.0;
    delta=root1;
    while(delta<root2){
      psit=psiFromDelta(delta);
      if(psit>psimax){
	psimax=psit;
	deltamax=delta;
      }
      delta+=dstep;
    }
       
     dstep/=10.0;
     count++;
  }
  return psimax;
}

//get the two values of delta that give a value of psi
vector<double> Reionise::deltaFromPsi(double psi)
{
   double root1(-20.0);
   double root2(20.0);
   double psi1,psi2,psimax;
   double psiUnob(0);
   double delta1, delta2, deltamax(0.0);
   double tol(1.0e-3);
   vector<double> roots;
   

   psiUnob =getPsiUnobtain();
   if ( (psiUnob>=0.0) && (psi>psiUnob) ) {
     fprintf(stderr,"\nReionise::deltaFromPsi -> psi=%f > Psi= %f that we've already established cannot be obtained by any value of delta at redshift = %.2f\n",psi, psiUnob, getz());
     return roots;
   }
       //test limits
   psi1=psiFromDelta(root1);
   psi2=psiFromDelta(root2);
     
   
   if(psi1>psi || psi2>psi){
     fprintf(stderr,"\nReionise::deltaFromPsi -> limits insufficient\n");
     return roots;
   }

   //Check if the hard code data has been generated yet, and if it has then make use of it.
   if (gethard_code_gen()==1) {
     if (DEBUG>=3) fprintf(stderr, "\nReionise::deltaFromPsi -> Using hard coded data\n");
     getPsiMax(psimax);
     getDeltaMax(deltamax);
   }
   else {
     if (DEBUG>=3) fprintf(DBUG, "\nReionise::deltaFromPsi -> Not using hard coded data\n");
     psimax = calcPsiMax(deltamax,psi,root1,root2);
   }

   if(psi>psimax){
     fprintf(stderr,"\nReionise::deltaFromPsi -> Psi = %.2f is unobtainable for any delta at this redshift, -> P(%.2f) = 0\n", psi,psi);
     setPsiUnobtain(psi);
     return roots;
   }

   //can now use psimax for two rootfinding exercises
   setpsiFromDelta(deltamax,this,1);
   delta1=zriddrConst(dummypsiFromDelta,psi,root1,deltamax,tol);
   delta2=zriddrConst(dummypsiFromDelta,psi,deltamax,root2,tol);
   
   roots.clear();
   roots.push_back(delta1);
   roots.push_back(delta2);
   return roots;
}

/* psiFromDelta -> From eqn 10 FZH04*/
double Reionise::psiFromDelta(double deltaM)
{
  double Psi;
 
  Psi = (1.0 +  (deltaM*C->growthFac(getz())) )*xH(deltaM);
  
  return Psi;
}

/* xH -> Neutral fraction using ionised frac as 
   per eqn 5 in Furlanetto & Oh 2005 */
double Reionise::xH(double deltaM)
{
  double xH, xi,fcoll;

  fcoll = fcollExPS(deltaM);

  xi = ZETA*fcoll;
  if (xi>1.0) xi=1.0;
  if(xi<0.0) xi=0.0;
  xH = 1.0 - xi;

  if (DEBUG>=4) fprintf(DBUG, "\n----- In xH(deltaM)-----\n deltaM = %6.4f \t  xH = %6.4f \t fcoll = %6.4f \t xi = %6.4f \t Reionise::xH\n",deltaM, xH, fcoll,xi);

  return xH;
}

/* Collapse fraction according to extended press schechter, 
   eqn 3 FZH04 */
double Reionise::fcollExPS(double deltaM)
{
  /*
    Returns the extended Press-Schchter collapse function
    for a pixel. 
    Note you must have setsigpix previously!
    If you don't know the overdensity of your region, it
    is best to set deltaM to be the average overdensity,
    i.e. 0.
   */
  double sigmaMmin2,sigmaM2, x, fcoll;

  sigmaMmin2=pow(sigmaMmin(),2.0);
  sigmaM2 = getsigpix(); 
    
  // fcoll eqn 3 in FZH04
  x= ((C->delCrit0(getz())/C->growthFac(getz())) 
    - deltaM)/sqrt(2*(sigmaMmin2-sigmaM2)); //*/

  fcoll = erfcc( x ); 
  if (fcoll>1.0) fcoll=1.0;

  if (DEBUG>=4) fprintf(DBUG, "\n----- In fcollExPS(deltaM)-----\n deltaM = %6.4f \t x= %6.4e fcoll = %6.4e\t Reionise::fcollExPS \n",deltaM, x, fcoll);

  return fcoll;
}

//dDeltadPsi START
double Reionise::dPsidDelta(double deltaM)
{
  double deriv, err;

  setpsiFromDelta(0.0,this,1);
  deriv = dfridr(dummypsiFromDelta,deltaM,0.001, &err);
  if (DEBUG>=3) fprintf(DBUG, "\n----- In dPsidDelta(deltaM)-----\n dPsidDelta = %e \t Reionise::dPsidDelta\n",deriv);
  return deriv;
}
double setpsiFromDelta(double deltaM, Reionise *R1, int iflag)
{
  static Reionise *r;

  if(iflag==1){
    r=R1;
    return 0.0;
  }
  return r->psiFromDelta(deltaM);
}
double dummypsiFromDelta(double deltaM)
{
  return setpsiFromDelta(deltaM,NULL,0);
}
//dDeltadPsi END

double Reionise::MFromDelta(double deltaM)
{
  double meanrho, M, Vpix;
  
  meanrho = CRITDENMSOLMPC*C->getOm0hh();
  Vpix = getMpix()/meanrho;
  M = Vpix*( 1.0+(deltaM*C->growthFac(getz())) )*meanrho;
  return M;
} 

/* sigmaMmin() - Returns Sigma(Mmin) using coolMass in 
   dcosmology to calculate the minimum mass 
   for ionising sources */
double Reionise::sigmaMmin()
{
  double sigmaMmin,dummy,Mmin;

  Mmin = coolMass(C,getz());
  //cout<<"Mmin in sigmaMmin= "<<Mmin<<"\t"<<getz()<<endl;
  sigmaMmin= C->sigma0fM(Mmin,dummy,0);
  return sigmaMmin;
}

double Reionise::PpsiPionInt(double deltaM)
{
  return P_delta(deltaM)*P_ion(deltaM);
}

double setPpsiPionInt(double deltaM, Reionise *R1, int iflag)
{
  static Reionise *r;

  if(iflag==1){
    r=R1;
    return 0.0;
  }
  return  r->PpsiPionInt(deltaM);
}

double dummyPpsiPionInt(double deltaM)
{
  return setPpsiPionInt(deltaM,NULL,0);
}

double Reionise::MassFromVol(double V)
{
  // returns mass in units of solar mass given a Volume in Mpc^3
  double M;

  M=V*(CRITDENMSOLMPC*C->getOm0hh());
  return M;
  }

double Reionise::RfromM(double M)
{
  /* Returns the radius corresponding to a given mass
     corresponding to the mean density of the Universe*/
  return pow(  ( M/(CRITDENMSOLMPC*C->getOm0hh())*3.0/(4.0*PI) ),1.0/3.0  );
}



/* ----------------------------------------------------------------
Functions involved in calculating the neutral fraction (eqn 8 FZH04) 
 ----------------------------------------------------------------*/

double Reionise::dndlmFZH(double M)
{
  //implements Eqn 7 in FZH04, an adaption of the Sheth mass fn to the mass funciton of ionised bubbles but in terms of dn/d(lnM) = M*dn/dm = \rho*V*dn/dm
  double dndlm, nu, dlsdlM;
  double dsdM, sigM;
  
  //cout<<"K="<<getK()<<endl;

  sigM = C->sigma0fM(M,dsdM,1);
  dlsdlM = M*dsdM/sigM;
  nu=Barrier(pow(sigM,2.0))/sigM;

  dndlm = sqrt(2.0/PI)*CRITDENMSOLMPC*C->getOm0hh()/M;
  dndlm*=fabs(dlsdlM);
  dndlm*=B0(getK(), sigmaMmin())/sigM;
  dndlm*=exp(-pow(nu,2.0)/2.0);

  return dndlm;
}

double Reionise::Qbar()
{
  // eqn 8 FZH04, wrapper for calculting the neutral fraction  
  double mMin, mMax;
  double ans, oldAns,tol(1.0e-4);

  mMin = ZETA*coolMass(C,getz());
  ans = 0.0;
  //cout<<"z in Qbar"<<getz()<<endl;
  setIntQbar(0.0,this,1);
  while (1) {
    mMax = mMin*10.0;
    oldAns = ans;
    ans+=qromb(dummyIntQbar,mMin,mMax, tol);
    if (fabs(ans-oldAns)/ans < tol)
      break;
    mMin = mMax;
  }
  return ans/(CRITDENMSOLMPC*C->getOm0hh());
}
double Reionise::IntQbar(double M)
{
  return dndlmFZH(M);
}
double setIntQbar(double M, Reionise *R1, int iflag)
{
  static Reionise *r;

  if(iflag==1){
    r=R1;
    return 0.0;
  }
  return  r->IntQbar(M);
}
double dummyIntQbar(double M)
{
  return setIntQbar(M,NULL,0);
}



  /*************************************************
   ************ FZH04 STATISTICS FUNCTIONS *********
   *************************************************/

void Reionise::MuFZH()
{
  double PsiMx;
  double tol(1.0e-4);

  getPsiMax(PsiMx);
  setIntMuFZH(0.0,this,1);
  MUFZH=qromo(dummyIntMuFZH,0.0,PsiMx, midpnt,tol);
}

double Reionise::MomentFZH(int n)
{
  double moment, PsiMx;
  double tol(5.0e-4);
  
  if (getMuFZH()<0.0) {
    fprintf(stderr,"You have not called the MuFZH routine which calculates and stores the value of Mu for any given redshift. Please do so before trying to calculate any mooments");
    return -1;
  }
  setMomentFZH(n); //Set the n so the integral calculates the correct moment
  getPsiMax(PsiMx); //Establish Psi Max for this redshift

  setIntMomentFZH(0.0,this,1);
  moment=qromo(dummyIntMomentFZH,0.0,PsiMx,midpnt,tol);
  return moment;
}

void Reionise::SizeDist(double *A, double *nf)
{
  double R, VoverQdndlR, z;
  char filename[500], outputdir[500];
  FILE *F;

  sprintf(outputdir, "mkdir Output_files/SizeDistribution_Zeta%.2f_Mpix%e", ZETA,getMpix());
  system(outputdir);
  sprintf(outputdir, "Output_files/SizeDistribution_Zeta%.2f_Mpix%e", ZETA,getMpix());

  z=ZSTART;
  for (int i=0;i<NUMzBINS;i++) {
    // Open a file for the Prob Dist of Psi at z
    setA(*(A + i));
    setz(z);
    sprintf(filename, "%s/sizeDistribution_z%.2f_nf%f_A%f", outputdir, getz(),*(nf+i),getA());
    F = fopen(filename, "w");
    if (!F)
      fprintf(stderr, "Reionise::SizeDist -> Couldn't open file %s for writting!\n", filename);

    R=0.1;
    while (R<100) {
      VoverQdndlR=3*dndlmFZH(MfromRCom(R,C))*MfromRCom(R,C)/(CRITDENMSOLMPC*C->getOm0hh());//
      if (getBtype()==1)
	VoverQdndlR*=pow( *(nf+i),-1);
      else
	VoverQdndlR*=pow( (1.0-*(nf+i)),-1);//
      //
      fprintf(F, "%f\t%f\n", R, VoverQdndlR);
      R+=0.1;
    }
    z+=0.25;
    fclose(F);
    fprintf(stderr, "Size distribution for z=%.2f written to %s\n",getz(),filename);
  }//*/
}
void Reionise::PDFtoPlot(double *A)
{
  double z, psi, Ppsi;
  char filename[500], outputdir[500];
  FILE *F;

  sprintf(outputdir, "mkdir Output_files/PDF_psi_Zeta%.2f_Mpix%e", ZETA,getMpix());
  system(outputdir);
  sprintf(outputdir, "Output_files/PDF_psi_Zeta%.2f_Mpix%e", ZETA,getMpix());

  z=ZSTART;
  for (int i=0;i<NUMzBINS;i++) {
    setA(*(A + i));

    setz(z);
    // Open a file for the Prob Dist of Psi at z
    sprintf(filename, "%s/Ppsi_z%.2f_Zeta%.2f_Mpix%e", outputdir, getz(), ZETA,getMpix());
    F = fopen(filename, "w");
    if (!F)
      fprintf(stderr, "Reionise::PDFtoPlot -> Couldn't open file %s for writting!\n", filename);
    psi=0.0;
    for (int i=0;i<50;i++) {
      if ((getPsiUnobtain()>=0.0) && (psi>getPsiUnobtain())) {
	//If we've gone beyond psi_max then don't bother calculating higher values
	break;
      }
      Ppsi = pdfPsi(psi);
      fprintf(F, "%f\t%f\n", psi, Ppsi);
      psi+=0.025;
    }
    fclose(F);
    fprintf(stderr, "PDF for z=%.2f written to %s",getz(),filename);
    z+=0.25; //If you change this interval to anything that 0.25 is not a factor of you will need to calculate P(psi=0) and PsiMax as the hardcoded values are for 12<z<22.?? in intervals of 0.25
    }
}
  
/*********************************************************************
 * clustering statistics 
 *********************************************************************/
double Reionise::clusCorr(double mass, double r)
{
  double eta;
  //cout<<"calculalating linear bias:\n";
  eta=linBias(-1); //if arg < 1 then using minimum cooling mass.
  //cout<<linBias(-1)<<"\t now calculalating bubble bias:\n";
  eta*=bubbleBias(mass);
  //cout<<bubbleBias(mass)<<"\t now calculalating density correlation bias:\n";
  eta*=denCorr(r);
  //cout<<denCorr(r)<<endl;
  return eta;
}

double Reionise::linBias(double mMin) 
{
  // clustering correlation function eqn 20 FZH04

  /* If mMin < 0.0, assumes it is 
   * minimum cooling mass (this is an optional parameter).
   *
   * factor of 1.0e7 is a hack to make the integrator deal with 
   * the large masses involves... otherwise just whinges that 
   * the function doesn't diverge.   */

  double result, error, Q;
  double rel_tol  = 1.0e-4; //<- relative tolerance
  double size_lmt (1.0e8);  

  gsl_function F;
  gsl_integration_workspace * w 
    = gsl_integration_workspace_alloc (size_lmt);
  F.function = &intLinBias;
  struct linBias_params params = {this};
  F.params = &params;
  if (mMin < 0.0)
    mMin = coolMass(C, getz());
  mMin*=ZETA;
  mMin/=Mscale;
  gsl_integration_qagiu(&F, mMin, 0, rel_tol,size_lmt , w, &result, &error); 
  gsl_integration_workspace_free (w);

  Q=ZETA*C->fCollPSExact(getz(),-1);

  return result/Q/(CRITDENMSOLMPC*C->getOm0hh());
}

double intLinBias(double mass, void *p)
{
  struct linBias_params * params = (struct linBias_params *)p;
  Reionise *r = (params->r1);
  double ans;

  mass*=Mscale; 
  ans=r->dndlmFZH(mass)*r->bubbleBias(mass);
  //cout<<ans<<endl;
  return ans*Mscale; 
}

double Reionise::bubbleBias(double mass) //eqn 22 FZH04 (from Sheth & Tormen 2002)
{
  double sig2, b;
  double dummy;

  sig2 = pow(C->sigma0fM(mass,dummy,0),2.0);
  //cout<<pow(  B0( getK(), sigmaMmin() ), 2.0  )<<"\t"<<Barrier(sig2)<<"\t"<<sig2<<endl;
  b = 1.0 + pow(  B0( getK(), sigmaMmin() ), 2.0  )/( sig2*Barrier(sig2) );
  return b;
}

double Reionise::denCorr(double r)
{
  double result, error;
  double rel_tol  = 1.0e-4; // <- relative tolerance
  double size_lmt (1.0e8);  

  //set up the integration function, parameters and work space
  gsl_function F;
  gsl_integration_workspace * w 
    = gsl_integration_workspace_alloc (size_lmt);
  F.function = &intDenCorr;
  struct DenCorr_params params = {C, r, getz()};
  F.params = &params;

  //shut off the default error handler
  gsl_error_handler_t * pDefaultHandler; 
  const char * gsl_strerror (const int gsl_errno);
  pDefaultHandler= gsl_set_error_handler_off(); //we have to turn this off because it breaks down as the integral heads to zero (which should be perfetly acceptable).
  
  int status = gsl_integration_qagiu(&F, 0.0, 0, rel_tol, size_lmt, w, &result, &error);
  if (status) { /* an error occurred */
    /* status value specifies the type of error */
    if ( !(-0.01<=result)&& !(result<=0.01) ) {
	printf("error: %s .... ABORTING!!! \n", gsl_strerror (status));
	return -1;
    }
  }
  gsl_integration_workspace_free (w);

  return result;
}

double intDenCorr(double k, void *p)
{
  // integral over k to calculate DM density correlation (eqn 24 FZH04b)
  struct DenCorr_params * params = (struct DenCorr_params *)p;
  Cosmology *c = (params->c1);
  double r= (params->r12);
  double z= (params->z1);
  double ans;
  
  //cout<<k<<"\t"<<r<<"\t"<<z<<endl;
  ans = pow(k,2.0)/2.0/pow(PI,2.0);
  ans*=( sinc(k*r) );
  //cout<<sinc(k*r)<<endl;
  ans*=(  c->powerSpectrum(k)*pow(c->growthFac( z ),2.0)  );

  //cout<<ans<<endl;

  return ans;
  //return c->powerSpectrum(k)*pow(c->growthFac( z ),2.0);
}

  /*************************************************
   ****** FZH04 STATISTICS HELPER FUNCTIONS *******
   *************************************************/
double Reionise::IntMuFZH(double psi) 
{
  double Ppsi(0.0);

  if (psi>=0.0) {//>=0.02) {
    Ppsi = P_psi(psi)*psi;//pdfPsi(psi)*psi;
  }
  if (DEBUG>=3) fprintf(stderr,"Psi = %f, P(Psi) = %e and Psi*P(Psi) = %e\n",psi, P_psi(psi), Ppsi);
  return Ppsi;
}
double dummyIntMuFZH(double psi) 
{
  return setIntMuFZH(psi,NULL,0);
}
double setIntMuFZH(double psi, Reionise *R1, int iflag)
{
  static Reionise *r;

  if(iflag==1){
    r=R1;
    return 0.0;
  }
  return  r->IntMuFZH(psi);
}

double Reionise::IntMomentFZH(double psi)
{
  double moment(0.0);

  if (psi>=0.0) {//>=0.02) {
    moment=P_psi(psi)*pow( (psi-getMuFZH()), getMomentFZH() );//pdfPsi(psi)*pow( (psi-getMuFZH()), getMomentFZH() );
  }
  if (DEBUG>=3) fprintf(stderr,"z= %.2f, psi =%f, %ith moment=%e, & Mu=%e \n", getz(), psi, getMomentFZH(), moment, getMuFZH());
  return moment;
} 
double dummyIntMomentFZH(double psi)
{
  return setIntMomentFZH(psi,NULL,0);
}
double setIntMomentFZH(double psi, Reionise *R1, int iflag)
{
  static Reionise *r;

  if(iflag==1){
    r=R1;
    return 0.0;
  }
  return  r->IntMomentFZH(psi);
}

  /****************** ALGORITHMS INVOLVED IN GENERATING *************
   **********************LOADING HARD CODED VALUES OF ***************
   ********************* P(psi=0) & P_max, delta_max****************/
void Reionise::generateHCdata() 
{
  char filename[500];
  FILE *F;
  double z, psimx[NUMzBINS], deltamx, norm;
  
  //PsiMax Hard code data generation (if necessary) and loading:
  z=ZSTART;
  sprintf(filename, "./HARDCODE_DATA_FILES/PsiMaxDist_zstart%.2f_zeta%.2f_Mpix%e.dat", ZSTART, ZETA, getMpix());
  F=fopen(filename, "r");
  if(F==NULL){
    F=fopen(filename, "w");
    fprintf(stderr, "can not open %s, assuming that it therefore does not exist and am generating new hardcoded file\n", filename);
    //Generate new PsiMax distribution hardcoded data file
    for (int i=0;i<NUMzBINS;i++) {
      setz(z);
      psimx[i] = calcPsiMax(deltamx,200.0,-20.0,20.0);
      fprintf(F, "%f\t%f\n",psimx[i], deltamx);
      fprintf(stderr, "\ngenerateHCdata --> PsiMax for z=%.2f written\n",z);
      z+=0.25;
    }
    fclose(F); //close the file*/
  }
  loadHC_PsiMax(); //loads the PsiMax hard-code data
  sethard_code_gen(1); //sets a flag so that all subsequent routines know the hard code data is generated and ready to use! We set this so that P0 knows to make use of it.

  //P(psi=0) Hard code data generation (if necessary) and loading:
  //Reset the redshift to Zstart
  z=ZSTART;
  sprintf(filename, "./HARDCODE_DATA_FILES/Psi0dist_zstart%.2f_zeta%.2f_Mpix%e.dat", ZSTART, ZETA, getMpix());
  F=fopen(filename, "r");
  if(F==NULL){
    F=fopen(filename, "w");
    fprintf(stderr, "can not open %s, assuming that it therefore does not exist and am generating new hardcoded file\n", filename);
    //Generate new P(psi=0) distribution hardcoded data file
    for (int i=0;i<NUMzBINS;i++) {
      setz(z);
      getPsiMax(psimx[i]);
      normPDF(norm,psimx[i]);
      fprintf(F, "%f\n",getP_psiZero()); 
      fprintf(stderr, "\ngenerateHCdata --> P(psi=0) for z=%.2f written\n",z);
      z+=0.25;
    }
    fclose(F);//close the file
  }
  loadHC_Psi0();

}



void Reionise::loadHC_PsiMax() 
{
  int i, n;
  FILE *fp;
  char filename[500];

  sprintf(filename, "./HARDCODE_DATA_FILES/PsiMaxDist_zstart%.2f_zeta%.2f_Mpix%e.dat", ZSTART, ZETA,getMpix());
  fp=fopen(filename, "r");
  if(fp==NULL){
    printf("can not open PsiMaxDist.dat\n");
    exit(1);
  }
  n=0;
  while (fscanf(fp, "%*e\t %*e")!=EOF)
    n++;
  rewind(fp);

  if ((n>NUMzBINS)||(n<NUMzBINS)) fprintf(stderr,"\nHey, what gives? There's a problem with the reading in of PsiMaxDist_zstart%.2f_zeta%.2f.dat. Should be %i elements and there is not\n", ZSTART, ZETA, NUMzBINS);

  for(i=0; i<n; i++){
    fscanf(fp, "%le\t %le", &(PsiMax[i]), &(DeltaMax[i]));
  }
  fclose(fp);
}

void Reionise::loadHC_Psi0()
{
  int i, n;
  FILE *fp;
  char filename[500];

  sprintf(filename, "./HARDCODE_DATA_FILES/Psi0dist_zstart%.2f_zeta%.2f_Mpix%e.dat", ZSTART, ZETA,getMpix());
  fp=fopen(filename, "r");
  if(fp==NULL){
    printf("can not open Psi0dist.dat\n");
    exit(1);
  }
  n=0;
  while (fscanf(fp, "%*e")!=EOF)
    n++;
  rewind(fp);

  for(i=0; i<n; i++) {
    fscanf(fp, "%le", &(PpsiZero[i]));
  }
  fclose(fp);
  
  if ((n>NUMzBINS)||(n<NUMzBINS)) fprintf(stderr,"\nHey, what gives? There's a problem with the reading in of Psi0dist_zstart%.2f_zeta%.2f..dat. Should be %i elements and there is not\n", ZSTART, ZETA,NUMzBINS);
}


/*********************************************************************
 * Functions involved in calculating 21cm statistics from MHR00 model*
 * You need to pass a COPY of the density box
 * if you wish to make further use of the density field
 * (use copyBox in tocmfast.cc)
 *********************************************************************/

float Reionise::Delta_ION(float *box, int Ntot, double nf, int InvMHR)
{
/*Calculates the  ionisation threshold discussed in
  MHR00 appropriate for given neutral fraction*/

  int M(0);
  
  setnf(nf);
  /*What is the number of pixels contributing to the ionised fraction?
   minus 1 as c++ counts from 0*/
  M = (1.0-nf)*Ntot - 1;
  /*Sort the density box into ascending order*/
  qsort(box,Ntot,sizeof(float), compare_flts);
  /*Find the Mth density pixel counting from low densities upwards. This
    corresponds to the density threshold (DELTA_ION)*/
  if (DEBUG<=2) fprintf(DBUG,"The critical density for ionisation is %.3f this corresponds to the %ith pixel of the %i total pixels when ordered in ascending order", box[M], M, Ntot);
  // SET THE DENSITY THRESHOLD
  if (InvMHR) {
    fprintf(stderr,"Performing inverse MHR analysis, i.e. if DELTA_pix>DELTA_ION then is ionised\n");
    setDeltaIon(box[Ntot-M]); 
    return box[Ntot-M];
  }
  else {
    setDeltaIon(box[M]);  
    return box[M];
  }
}


void Reionise::mk_loc_IonBox(float *xHbox, float *densitybox, int Ntot, int InvMHR)
{
/* for thelocalzed MHR and InvMHR models this
   flags pixels as neutral or ionised according to whether
   they is greater or less than the threshold returned by getDelta_ION*/
  float threshold;
  double ave (0);
 
  threshold = getDeltaIon();
  if (threshold==-10.0) fprintf(stderr, "The density threshold for ionisation has not been set. You need to run Delta_ION(float *box, int Ntot, double nf) before running this routine\t");
  if (InvMHR) {
    for (int i=0; i<Ntot; i++){
      if (densitybox[i]>threshold) xHbox[i]=0.0;
      else xHbox[i]=1.0;
      ave+=xHbox[i];
    }
  }
  else {
    for (int i=0; i<Ntot; i++){
      if (densitybox[i]<threshold) xHbox[i]=0.0;
      else xHbox[i]=1.0;
      ave+=xHbox[i];
    }
  }
  ave/=Ntot;
  if (fabs(ave-getnf())>1e-2) fprintf(stderr,"Reionise::mk_loc_Ionbox -> we are not correctly reproducing the input neutral fraction that was used in defining the threshold density value. neutral fraction should be = %.f, but we're producing a xH box with and average neutral fraction of %.f\n", getnf(), ave);
  cout<<getz()<<"\t"<<threshold<<"\t"<<ave<<"\t"<<getnf()<<endl;
}

int Reionise::mk_glob_IonBox(float *xH, char *Fname, int Ntot, int InvMHR)
{
  /*please note that this only works with the updated_smoothed boxes that 21cmfast outputs by default, you should not try to use it on a box you have smoothed in such a way as to remove padding.
    xH should be a real box to hold the xH field, Fname should contain a densitybox filename*/
  FILE *F;
  float cell_length_factor, delta;
  double R, dummy;
  double erfc_num, erfc_den, *fcoll;
  int LAST_FILTER_STEP;
  fftwf_complex *deltax_unfiltered, *deltax_filtered; 
  fftwf_plan plan;
 
  cell_length_factor = L_FACTOR;
  
  //CW allocate memory for the two filtered and unfiltered density fields
  deltax_unfiltered = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*HII_KSPACE_NUM_PIXELS);
  deltax_filtered = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*HII_KSPACE_NUM_PIXELS);

  if (!deltax_unfiltered || !deltax_filtered){
    fprintf(stderr, " Reionise::mk_glob_IonBox->Error allocating memory for deltax boxes\nAborting...\n");
     return -1;
     fftwf_free(deltax_unfiltered);fftwf_free(deltax_filtered);
  }

  fcoll=(double *) malloc(sizeof(double)*HII_TOT_NUM_PIXELS);
  if (!fcoll){
    fprintf(stderr, " Reionise::mk_glob_IonBox->Error allocating memory for fcoll\nAborting...\n");
     return -1;
     fftwf_free(fcoll);
  }

  F = fopen(Fname, "rb");
  if (!F){
    fprintf(stderr, "Reionise::mk_glob_IonBox-> Unable to open file: %s\n", Fname);
    fftwf_free(deltax_unfiltered); fftwf_free(deltax_filtered);
    return -1;
  }

  //CW load the updated_smoothed box to delta_unfiltered
  if (mod_fread(deltax_unfiltered, sizeof(fftwf_complex)*HII_KSPACE_NUM_PIXELS, 1, F)!=1){
    fprintf(stderr, "Reionise::mk_glob_IonBox-> Read error occured while reading deltax box.\n");
    fftwf_free(deltax_unfiltered); fftwf_free(deltax_filtered);
    return -1;
  }
  fclose(F);
  fprintf(stderr,"\n---------In mk_glob_IonBox, initialising xH box, InverseMHR=%i--------\n",InvMHR);

  //CW if regions that have crossed barrier are to be ionised then initialise the neutral fraction to be 1 everywhere
  for (unsigned int ct=0; ct<HII_TOT_NUM_PIXELS; ct++){    xH[ct] = 1; }

 // if (InvMHR==1) {
    //setBtype(0); //CW normal FZH barrier (inverse MHR roughly the same as 21cmfast)

  //}
  //CW if regions that have NOT crossed barrier are to be ionised then initialise the neutral fraction to be 0 everywhere
  /*else {
    setBtype(1); //CW use the MHR barrier w/negative slope
    for (int ct=0; ct<HII_TOT_NUM_PIXELS; ct++){    xH[ct] = 0;  }
  }*/

  //CW FFT unfiltered field to k-space
  plan = fftwf_plan_dft_r2c_3d(HII_DIM, HII_DIM, HII_DIM, (float *)deltax_unfiltered, (fftwf_complex *)deltax_unfiltered, FFTW_ESTIMATE);
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);
  fftwf_cleanup();
  // remember to add the factor of VOLUME/TOT_NUM_PIXELS when converting from
  //  real space to k-space
  // Note: we will leave off factor of VOLUME, in anticipation of the inverse FFT below
  for (unsigned int ct=0; ct<HII_KSPACE_NUM_PIXELS; ct++){
    //CW divide every pixel value by total pixels for FFT
     deltax_unfiltered[ct] /= (HII_TOT_NUM_PIXELS+0.0);
  }
  //CW set initial R by which ever is smaller, 30Mpc or the radius corresponding the size of the box. Each loop R /= DELTA_R_HII_FACTOR
  R=fmin(R_BUBBLE_MAX, L_FACTOR*BOX_LEN);
  LAST_FILTER_STEP = 0;

  /*//CW ***REDUNDANT*** Load the Barrier offset A 
    (note you will need to have run FZH first to establish 
    the A values needed to give us roughly the
    same neutral fractions as the 21CMFAST setup, by default creates a file in
    the Output folder of this program that contains this info*/

  //loadHC_A(A); //CW load the value of A relevant to our redshift
  //setA(A); //CW set stored A so all slave routines can access it.
  fprintf(stderr,"\n---------In mk_glob_IonBox, starting density box smoothing from big to small. At each stage check to see if our choosen barrier is crossed, Barrier Type=%i and A = %.2f--------\n",getBtype(), getA());

  while (R > (cell_length_factor*BOX_LEN/(HII_DIM+0.0))){

    //cout<<R<<endl;
    memcpy(deltax_filtered, deltax_unfiltered, sizeof(fftwf_complex)*HII_KSPACE_NUM_PIXELS); //CW copy the unfiltered field to deltax_filtered variable
    fprintf(stderr,"\n---------In mk_glob_IonBox, Filtering on scale, R=%f Mpc--------\n",R);
    T0->HII_filter(deltax_filtered, HII_FILTER, R); //CW filters deltax_filtered on scale of this loop's R

    //CW FFT back to real place pwease
    plan = fftwf_plan_dft_c2r_3d(HII_DIM, HII_DIM, HII_DIM, (fftwf_complex *)deltax_filtered, (float *)deltax_filtered, FFTW_ESTIMATE);
    fftwf_execute(plan);
    fftwf_destroy_plan(plan);
    fftwf_cleanup();

    if ((R/DELTA_R_HII_FACTOR) <= (cell_length_factor*BOX_LEN/(float)HII_DIM)){
      //CW its the last filtering step, assigns partial ionisations
      LAST_FILTER_STEP = 1;
      fprintf(stderr, "Reionise::mk_glob_IonBox-> Last filtering step, LAST_FILTER_STEP=%i.\n", LAST_FILTER_STEP);
      F = fopen(Fname, "rb");
      if (mod_fread(deltax_unfiltered, sizeof(fftwf_complex)*HII_KSPACE_NUM_PIXELS, 1, F)!=1){ //CW reload the density box to deltax_unfiltered so its real
	fprintf(stderr, "find_HII_bubbles: Read error occured while reading deltax box.\n");
	fprintf(DBUG, "find_HII_bubbles: Read error occured while reading deltax box.\n");
	free(xH); fftwf_free(deltax_unfiltered); fftwf_free(deltax_filtered);
	return -1;
      }
      fclose(F);
      if (InvMHR==1)
	ST_PS_correct(deltax_unfiltered, MfromRCom(cell_length_factor*BOX_LEN/(float)HII_DIM, C ), fcoll); //CW correction for sheth-torman mass fcn, returns ST corrected fcoll values for every pixel
      else {
	setSTPScorr(1);
	//CW we need flow in order to be able to generate partial reionisations in the Global MHR model
	erfc_den=sqrt( 2*(pow(C->sigma0fM(coolMass(C,getz()),dummy,0), 2) - pow(C->sigma0fM(MfromRCom(R, C),dummy,0), 2) ) );
	for (int x=0; x<HII_DIM; x++){
	  for (int y=0; y<HII_DIM; y++){
	    for (int z=0; z<HII_DIM; z++){
	      delta= *((float *) deltax_unfiltered + HII_R_FFT_INDEX(x,y,z));
	      erfc_num = (C->delCrit0(getz()) + delta)/C->growthFac(getz()); 
	      *((double*) fcoll +HII_R_INDEX(x,y,z)) = erfc(erfc_num/erfc_den);
	    }
	  }
	}	

      }

      ioniseBox(xH, deltax_filtered, fcoll, R, 0, InvMHR); //CW this performs the usual ionisation step for final filtering scale
      ioniseBox(xH, deltax_unfiltered, fcoll,  cell_length_factor*BOX_LEN/(float)HII_DIM, LAST_FILTER_STEP, InvMHR); //CW this call allocates partial ionisations

    } //END of Last filter loop, 

    else {//CW it's not the last loop 

      if (InvMHR==1){
	fprintf(stderr,"\n---------In mk_glob_IonBox, Inverse MHR so calculating Sheth-Torman correction factor--------\n");
	ST_PS_correct(deltax_filtered, MfromRCom(R,C), fcoll);
      }
      else {
	setSTPScorr(1);
	//ST_PS_correct(deltax_filtered, MfromRCom(R,c), fcoll);
      }

      fprintf(stderr,"\n---------In mk_glob_IonBox, Ionise/neutralise box--------\n");
      ioniseBox(xH, deltax_filtered, fcoll, R, LAST_FILTER_STEP, InvMHR);
    }
    
    if (DEBUG>=1) fprintf(stderr,"Reionise::mk_glob_IonBox->smoothing R=%f\n",R);
    R /= DELTA_R_HII_FACTOR; //CW reduces smoothing R for the next scale
  }
  fftwf_free(deltax_filtered); fftwf_free(deltax_unfiltered); //don't need this anymore.
  free(fcoll);
  //check to see how out our neutral field is from that of the input neutral fraction and correct by setting a new threshold according to the required correction
  T0->setave(T0->averagebox(xH,1,0));
  cout<<"average neutral frac="<<T0->getave()<<"\tshould be "<<T0->getnf()<<endl;
  return 0;

}

void Reionise::ioniseBox(float *xH, fftwf_complex *densitybox, double *fcoll, double R, int LAST_STEP, int InvMHR) 
{
//assigns ionisation level of xH box according to a threshold test
  double dummy, threshold, Sigma, Sigma_min;
  double ST_over_PS, density_over_mean;
  double pixel_mass, M, M_min;
  float deltax, ave_N_min_cell, res_xH;
  int N_min_cell;

  const gsl_rng_type *T;
  gsl_rng *r;

  cout<<"LAST_STEP="<<LAST_STEP<<"\t R="<<R<<endl;
  // find the start neutral fraction
  double global_xH;
  global_xH = 0;
  for (unsigned int ct=0; ct<HII_TOT_NUM_PIXELS; ct++){
    global_xH += xH[ct];
  }
  global_xH /= (float)HII_TOT_NUM_PIXELS;
  cout<<"starting neutral frac="<<global_xH<<"\n";

  M=MfromRCom(R, C);

  ST_over_PS = getSTPScorr();

  cout<<"ST_over_PS (@z="<<getz()<<") is "<<ST_over_PS<<endl;
  setK(inverf(1.0-pow((ZETA*ST_over_PS),-1.0)));//include correction for sheth torman in the K(zeta) we use in our barrier

  pixel_mass = MfromRCom((L_FACTOR*BOX_LEN/(float)HII_DIM),C); 
  M_min=coolMass(C,getz());

  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc(T);

  Sigma=C->sigma0fM( M,dummy,0 );
  Sigma_min=C->sigma0fM(M_min,dummy,0);

  cout<<"we set K in ioniseBox to be: "<<inverf(1.0-pow((ZETA*ST_over_PS),-1.0))<<"and for a sigma^2 of"<<Sigma*Sigma<<endl;

  //calculate the barrier height for sigma(M) incl correction for the sheth-torman mass fn (resulting from K). This is the exact version of the linear barrier we use in the FZH model
  threshold=(  (C->delCrit0(getz())/C->growthFac(getz())) - getK()*sqrt( 2*(pow(Sigma_min,2) - pow(Sigma,2)) )  )*C->growthFac(getz()); //Full barrier (as oposed to linear approx), only useable for the InverseMHR analysis.

  if (!(InvMHR==1))
   threshold*=-1.0; 
       
//Barrier(pow(Sigma,2))*C->growthFac(getz());

//

  //cout<<getA()<<"\t"<<Sigma*Sigma<<"\t"<<threshold<<endl;
  
  for (int x=0; x<HII_DIM; x++){
    for (int y=0; y<HII_DIM; y++){
      for (int z=0; z<HII_DIM; z++){
	//load the density
	deltax = *((float *)densitybox + HII_R_FFT_INDEX(x,y,z));
	density_over_mean=1+deltax;

	/*if (InvMHR==1) //temp variable for part ionisation conditional in last step
	  res_xH=*((float *)xH+HII_R_INDEX(x, y, z)); 
	else
	  res_xH=1-*((float *)xH+HII_R_INDEX(x, y, z));//inverting so that we can use the same conditional on the next section*/
        res_xH=*((float *)xH+HII_R_INDEX(x, y, z)); 	
	//CW	if (!(LAST_STEP)&&(deltax > threshold)) {//if were working with inverse and the pixel has crossed barrier and if working in terms of Inverse MHR pixel  is ionised, if using MHR then it is neutral
	if (!(LAST_STEP)&& (  ( !(InvMHR==1)&&(deltax < threshold) )||( (InvMHR==1)&&(deltax >threshold) )  ) ) {//if were working with inverse and the pixel has crossed barrier and if working in terms of Inverse MHR pixel  is ionised, if using MHR then it is neutral
	  //cout<<deltax<<"should be >"<<threshold<<endl;
	  if (FIND_BUBBLE_ALGORITHM == 2 ) {// center method
	    /*if (InvMHR==1) 
	      *((float *)xH+HII_R_INDEX(x, y, z)) = 0;
	    else {
	      *((float *)xH+HII_R_INDEX(x, y, z)) = 1; */   
            *((float *)xH+HII_R_INDEX(x, y, z)) = 0;
	      //cout<<"MHR and "<<deltax<<">"<<threshold<<"so xH ="<<1<<endl;
	    }
	  
	  else if (FIND_BUBBLE_ALGORITHM == 1) // sphere method (never use this for the last step)
	    T0->update_in_sphere(xH, HII_DIM, R/BOX_LEN, x/(HII_DIM+0.0), y/(HII_DIM+0.0), z/(HII_DIM+0.0), 1);
	  else{
	    fprintf(stderr, "Reionise::mk_glob_IonBox->Incorrect choice of find bubble algorithm: %i\nAborting...", FIND_BUBBLE_ALGORITHM);
	    z=HII_DIM;y=HII_DIM,x=HII_DIM;R=0;
	    exit(1);
	  }
	}
	
	if ((LAST_STEP)&&(res_xH > TINY))
	  {
	    ave_N_min_cell = *((double *)fcoll + HII_R_FFT_INDEX(x,y,z)) * pixel_mass*density_over_mean / M_min; // ave # of M_MIN halos in cell
	    if (ave_N_min_cell < N_POISSON)
	      { 
		// the collapsed fraction is too small, lets add poisson scatter in the halo number
		N_min_cell = (int) gsl_ran_poisson(r, ave_N_min_cell);
		*((double *)fcoll +HII_R_INDEX(x,y,z) ) = N_min_cell * M_min / (pixel_mass*density_over_mean);
	      }
	    /*if (InvMHR==1)
	      res_xH = 1 - *((double *) fcoll + HII_R_INDEX(x,y,z) )*ZETA;
	    else
	      res_xH = *((double *) fcoll + HII_R_INDEX(x,y,z) )*ZETA;*/
            res_xH = 1 - *((double *) fcoll + HII_R_INDEX(x,y,z) )*ZETA;
	    // and make sure fraction doesn't blow up for underdense pixels

	    if ((res_xH > 0) && (res_xH <= 1)){
	      *((float *)xH + HII_R_INDEX(x, y, z)) = res_xH;
	    }
	  }	    
	  
      } // x
    } // y
  } // z


  // find the end neutral fraction
  global_xH = 0;
  for (unsigned int ct=0; ct<HII_TOT_NUM_PIXELS; ct++){
    global_xH += xH[ct];
  }
  global_xH /= (float)HII_TOT_NUM_PIXELS;
  cout<<"end neutral frac="<<global_xH<<"\n";
}


/*********************************************************************
 * MHR00 HELPER FUNCTIONS 
 ********************************************************************/
double Reionise::dndlmMHR(double M)
{

     //co-moving number density per unit mass for a uniform barrier*\rho_ave*V(M) 
  double dndlm, nu, dlsdlM;
  double dsdM, sigM;
  double del_ion;
  
  sigM = C->sigma0fM(M,dsdM,1);
  del_ion = getDeltaIon()/C->growthFac(getz());
  dlsdlM = M*dsdM/sigM;
  nu = del_ion/sigM;
  //Sig2= (pow(sigM,2.0)-pow(C->sigma0fM(MassFromVol(pow(BOX_LEN,3.0)),dsdM,0),2.0)); 
  //nu = del_ion/sqrt(Sig2);

  dndlm = sqrt(2.0/PI)*CRITDENMSOLMPC*C->getOm0hh()/M;
  dndlm*=(del_ion/sigM);
  dndlm*=fabs(dlsdlM);
  //dndlm*=del_ion;
  //dndlm*=( pow(sigM,2.0)/pow( Sig2,1.5 ) );
  dndlm*=exp(-pow(nu,2.0)/2.0);
  
  return dndlm;
}
double Reionise::IntxH(double M)
{
  return dndlmMHR(M); 
}
double setIntxH(double M, Reionise *R1, int iflag)
{
  static Reionise *r;

  if(iflag==1){
    r=R1;
    return 0.0;
  }
  return  r->IntxH(M);

}
double dummyIntxH(double M)
{
  return setIntxH(M,NULL,0);
}

void Reionise::loadHC_A(double &A)
{
  char filename[500];
  double Atmp[100];
  int i, n, array_indx;
  FILE *F;

  sprintf(filename, "../4_FZH2004/Output_files/A_Barrier_zeta%.2f_Zstart%.2f", ZETA,ZSTART);

  F=fopen(filename, "r");
  if(F==NULL){
    printf("cannot open %s\n", filename);
    exit(1);
  }
  //fprintf(stderr,"filename using to get A for barrier is %s\n",filename);

  n=0;
  while (fscanf(F, "%*e")!=EOF)
    n++;
  rewind(F);

  for(i=0; i<n; i++) {
    fscanf(F, "%le", &Atmp[i]); 
    //cout<<Atmp[i]<<endl;
  }
  fclose(F);

  //get appropriate index for the redshift of choice!
  if ( (getz()<ZSTART) || (getz()>(ZSTART+((NUMzBINS-1)*0.25))) ) {
    fprintf(stderr, " getHC_A-> You're trying to cal values outside of the range of redshifts corresponding to the hard cosed data you've loaded. Keep within range %.2f<z<%.2f in increments of 0.25\n", ZSTART, (ZSTART+((NUMzBINS-1)*0.25)));
    exit(1);
  }
  array_indx = (getz()-ZSTART)*4.0;
  A = Atmp[array_indx];
  //cout<<"A in loadHC_A="<<A<<"for z="<<getz()<<endl;
}

void Reionise::ST_PS_correct(fftwf_complex *densitybox, double M, double *fcoll_box)
{
  double mean_f_coll_st(0), mean_f_coll_ps(0);
  double delta, M_min, erfc_num, erfc_den;
  double dummy;

  M_min=coolMass(C,getz());
//corrects for sheth-torman (with Jenkins parameter fits)
  mean_f_coll_st = C->fColl(getz(),M_min, 2); //calculates the average collapsed fraction using the Jenkins et al 2001 mass fcn
  erfc_den=sqrt( 2*(pow(C->sigma0fM(M_min,dummy,0), 2) - pow(C->sigma0fM(M,dummy,0), 2) ) ); 

  // renormalize the collapse fraction so that the mean matches ST, 
  // since we are using the evolved (non-linear) density field
  for (int x=0; x<HII_DIM; x++){
    for (int y=0; y<HII_DIM; y++){
      for (int z=0; z<HII_DIM; z++){
	delta = *((float *)densitybox + HII_R_FFT_INDEX(x,y,z));
	erfc_num = (C->delCrit0(getz())  - delta)/C->growthFac(getz());
	*((double*) fcoll_box +HII_R_INDEX(x,y,z)) = erfc(erfc_num/erfc_den);
	mean_f_coll_ps+=*((double*) fcoll_box +HII_R_INDEX(x,y,z));
      }
    }
  }
  mean_f_coll_ps /= (double) HII_TOT_NUM_PIXELS;

  for (int x=0; x<HII_DIM; x++){
    for (int y=0; y<HII_DIM; y++){
      for (int z=0; z<HII_DIM; z++){
	*((double*) fcoll_box +HII_R_INDEX(x,y,z))*=mean_f_coll_st/mean_f_coll_ps; //correcting to sheth-torman/jenkins mass fcn
      }
    }
  }

  setSTPScorr(mean_f_coll_st/mean_f_coll_ps);
  //return mean_f_coll_st/mean_f_coll_ps;
}

/*********************************************************************
 * MHR00 HELPER FUNCTION (not group member)
 ********************************************************************/
int compare_flts(const void* a, const void* b)   // comparison function
{
    float* arg1 = (float*) a;
    float* arg2 = (float*) b;
    if (*arg1 < *arg2) return -1;
    else if (*arg1 == *arg2) return 0;
    else return 1;
}

/*************
 * TO UPDATE VALUES OF PROTECTED MEMBER VARIABLES 
 ************/
void Reionise::setz(double z)
{
  z1=z; // Updates the redshift
  PsiUnobtain=-10.0; //resets the PsiUnobtain variable if the redshift gets adjusted midrun (as this is dependent on redshift).
}
void Reionise::setMpix(double M)
{
  //M contained in a pixel in units of Msol
  MPIX=M;
  if (DEBUG>=4) fprintf(DBUG, "Setting Mpix = %e \n", M);
}
void Reionise::setdelta0(double delta0)
{
  deltaZero = delta0; // Updates the delta0 value
}
void Reionise::setnf(double nf)
{
  nf1 = nf;
}
void Reionise::setP_psiZero(double P_psi0)
{
  Ppsi0=P_psi0;
}
void Reionise::setPsiUnobtain(double Psi)
{
  PsiUnobtain=Psi;
}
void Reionise::setDeltaIon(float delIon)
{
  DELTAION=delIon;
}
void Reionise::setMuFZH(double Mu)
{
  MUFZH=Mu;
}
void Reionise::setMomentFZH(int Moment)
{
  MOMENT=Moment;
}
void Reionise::sethard_code_gen(int hard_code_flag) 
 {
   HARD_CODE_GEN = hard_code_flag;
 }
void Reionise::setA(double A)
{
  Aa=A;
}
void Reionise::setBtype(int B)
{
  Btype=B;
}
void Reionise::setK(double K)
{
  K1 = K;
}
void Reionise::setSTPScorr(double STPS)
{
  STPScorr=STPS;
}


/*************
 * ROUTINES TO ACCESS PROTECTED VARIABLES
************/
double Reionise::getz()
{
  return z1;
}

double Reionise::getMpix()
{
  return MPIX;
}
double Reionise::getdelta0()
{
  return deltaZero;
}

double Reionise::getsigpix()
{
  double sigpix;
  double dummy;

  sigpix = C->sigma0fM(getMpix(), dummy, 0);
  return sigpix;
}
 double Reionise::getnf()
 {
   return nf1;
 }
// P_psiZero -> Probability of psi=0 (i.e. pixel ionised)
double Reionise::getP_psiZero() 
{
  return Ppsi0;
}
double Reionise::getPsiUnobtain() 
{
  return PsiUnobtain;
}
double Reionise::getMuFZH()
{
  return MUFZH;
}
int Reionise::getMomentFZH()
{
  return MOMENT;
}
int Reionise::getPpsi0(double &PpsiZERO)
{
  int array_indx;

  if ( (getz()<ZSTART) || (getz()>(ZSTART+(39*0.25))) ) {
    fprintf(stderr, "getPpsi0 -> You're trying to cal values outside of the range of redshifts corresponding to the hard cosed data you're trying to use. Keep within range %.2f<z<%.2f in increments of 0.25\n", ZSTART, (ZSTART+(39*0.25)));
    return -1;
  }
  array_indx = (getz()-ZSTART)*4.0;
  PpsiZERO = PpsiZero[array_indx];
  return 1;
}

int Reionise::getPsiMax(double &PsiMx)
{
  int array_indx;

  if  ( (getz()<ZSTART) || (getz()>(ZSTART+(39*0.25))) ) {
    fprintf(stderr, "getPsiMax -> You're trying to cal values outside of the range of redshifts corresponding to the hard cosed data you're trying to use. Keep within range %.2f<z<%.2f in increments of 0.25\n", ZSTART, (ZSTART+(39*0.25)));
    return -1;
  }
  array_indx = (getz()-ZSTART)*4.0;
  PsiMx = PsiMax[array_indx];
  return 1;
}

int Reionise::getDeltaMax(double &DeltaMx)
{
  //double zstart(12.0);
  int array_indx;
 
  if ( (getz()<ZSTART) || (getz()>(ZSTART+(39*0.25))) ) {
    fprintf(stderr, "Your trying to cal values outside of the range of redshifts corresponding to the hard cosed data you're trying to use. Keep within range %.2f<z<%.2f in increments of 0.25\n", ZSTART, (ZSTART+(39*0.25)));
    return -1;
  }

  array_indx = (getz()-ZSTART)*4.0;
  DeltaMx = DeltaMax[array_indx];
  return 1;
}
int Reionise::gethard_code_gen()
{
  return HARD_CODE_GEN;
}

//MHR specific
float Reionise::getDeltaIon() 
{
  return DELTAION;
}

double Reionise::getA()
{
  return Aa;
}

int Reionise::getBtype()
{
  return Btype;
}
double Reionise::getK()
{
  return K1;
}

double Reionise::getSTPScorr()
{
  return STPScorr;
}

double sinc(double x)
{
  if (x==0)
    return 1;
  else
    return sin(x)/x;
}
