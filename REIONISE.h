/* REIONISE.h
 *
 * Includes utility functions for reionisation operations. CAW2012.
 * Nb. Uses many dcosmology.cc routines
 * 
 * REFERENCES:
 * Furlanetto, Zaldarriaga & Hernquist 2004 astro-ph/0403697 (hereafter FZH04)
 * Sheth & Torman 2002
 * Watkinson & Pritchard 2014a
 */

#ifndef REIONISE_H
#define REIONISE_H

#include "../Cosmo/dcosmology.h"
#include "TOCMFAST.h"
#include "../NR/spline.h"
#include <vector>

// USER ADJUSTABLE variables:

// ************ FZH USER ADJUSTABLE variables: ************
const double ZETA (16.0); //The ionising efficiency of sources used by FZH04
const double ZSTART (0); //Used for caclulating the appropriate index when accessing hard coded data.
/*Note minimum physical values for this model are:
Zeta=40 -> ZSTART=11.25
Zeta=31 -> ZSTART=10.75
Zeta=16 -> ZSTART=7.0 ~(or maybe even less)
*/
const int NUMzBINS (20);

// Constants:
const double Mscale (1.0e16); // M needs rescaling for integrating mass functions with gsl, this parameter determines by how much.
const float DEL_gal (178.0); // 1 + \delta for galaxies (if treating as virialised objects)

class Reionise {

 public:
  Reionise(Cosmology *c1,  Tocmfast *T01, double z, FILE *LOG);
  ~Reionise();
/*********************************************************************
                       SOBACHI & MESINGER SHIZZM
*********************************************************************/
  int galacticHI(float *xH, float *delta, float alpha, int res, float L); //adds alpha percent of galactic HI, based of fcoll

/*********************************************************************
* Functions involved in calculating 21cm probabilities from FZH04 model *
*********************************************************************/

  double pdfPsi(double psi); // Full pdf for psi  
  double P_psi(double psi); // PDF of non-zero psi distribution from FZH04 eqn 32
  // To calculate P_psi need:
  double P_ion(double delta0); // Probability a pixel is ionised, eqn29 FZH04 
  double P_crossB(double sigmaM2); /* eqn 31 */ 
  double Barrier(double sigmaM2); /* eqn 4-6 */
  double B0(double K, double sigMmin); /*  Eqn 5 FZH04 */
  double P_delta(double deltaM); // Prob of deltaM from delta PDF (assumed gaussian)

/* FZH04 HELPER FUNCTIONS  */
  std::vector<double> deltaFromPsi(double psi); //Calculate the 2 values of delta appropriate to passed value of psi
  double psiFromDelta(double deltaM); //From eqn 10 FZH04
  double xH(double deltaM); // Neutral fraction using ionised frac as per eqn 5 in Furlanetto & Oh 2005 
  double fcollExPS(double deltaM); //Collapse fraction according to extended press schechter, eqn 3 FZH04 
  double dPsidDelta(double deltaM); //Differential term of eqn 32, FZH04 
  double MFromDelta(double deltaM);
  double sigmaMmin(); //Returns Sigma(Mmin) using coolMass in dcosmology to calculate the minimum mass for ionising sources
  double PpsiPionInt(double deltaM);
  double MassFromVol(double V);// returns mass in units of solar mass given a Volume in Mpc^3
  double RfromM(double M);
  
/*********************************************************************
  Functions involved in calculating the NEUTRAL FRACTION Qbar 
  (using eqn 8 FZH04) 
*********************************************************************/

  double dndlmFZH(double M); //implements Eqn 7 in FZH04, adaption of the Sheth mass fn
  double Qbar(); // eqn 8 FZH04, wrapper for calculting the neutral fraction  
  double IntQbar(double M);

  /*************************************************
   ************ FZH04 STATISTICS FUNCTIONS *********
   *************************************************/

  void MuFZH(); //calculates mu by integrating P(psi)*psi
  double MomentFZH(int n); //calculates n'th moment by  integrating P(psi)*(psi-mu)^moment
  void SizeDist(double *A, double *nf);
  void PDFtoPlot(double *A); //Generates samples of the PDF for plotting at each redshift

  /*********************************************************************
   * Clustering 
   *********************************************************************/

  double clusCorr(double mass, double r); // clustering correlation of ionized region function eqn 20 FZH04
  double linBias(double mMin); // linear bias of ionised region of mass m, eqn 21 FZH04
  double bubbleBias(double mass); //eqn 22 FZH04 (from Sheth & Tormen 2002)
  double denCorr(double r); // DM density correlation function

  /*FZH04 STATISTICS HELPER FUNCTIONS */
  double IntMuFZH(double psi); 
  double IntMomentFZH(double psi); 


  /****************** ALGORITHMS INVOLVED IN GENERATING *************
   **********************LOADING HARD CODED VALUES OF ***************
   ********************* P(psi=0) & P_max, delta_max****************/

  void generateHCdata(); // Wrapper for generating the hard code data files
  double calcPsiMax(double &deltamax, double psi, double root1, double root2); //Return the maximum value of Psi for any given z.
  void normPDF(double &norm, double IntMaxLim); 
  // Forced normalisation of pdf by assuming the non-zero PDF of FZH04 is a well behaved PDF and therefore assigning any deficit from 1.0 to the probability of psi=zero. Note there are hard-coded values for this that can be obtained using loadHC_Psi0. 
  void loadHC_PsiMax(); 
  // Loads Psi Max to a protcted variable for use as upper limit when integrating the PDF corresponds to z=ZSTART to ZSTART+(39*0.25) (increments of 0.25). Also imports the corresponding delta_max
  void loadHC_Psi0(); 
  // Loads Probability of Psi=0 corresponds to z=ZSTART to ZSTART+(39*0.25)(increments of 0.25)


  /*********************************************************************
   * Functions involved in calculating 21cm statistics from 
   *                         MHR00 model 
   * Nb. I chose to work in terms of delta rather than DELTA (1.0+delta)
   * as 21CMFAST outputs the density field in terms of delta
   *********************************************************************/

  float Delta_ION(float *box, int Ntot, double nf, int InvMHR); /*Calculates the 
					       ionisation
					       threshold discussed in 
					       MHR00 appropriate for given
					       neutral fraction*/

  void mk_loc_IonBox(float *xHbox, float *densitybox, int Ntot, int InvMHR); /* flags pixels as 
							neutral or 
					      ionised according to whether
					      it is greater or less than the
					      threshold returned by 
					      getDelta_ION*/

  int mk_glob_IonBox(float *xH, char *Fname, int Ntot, int InvMHR); //this performs a similar function to find HII bubbles in 21CMFAST except it takes a ionising threshold as arguement and uses that to decide whether or not a pixel be ionised . 
  // NOTE: this is not really set up yet to work with the inverse MHR model, even though it appears to be
 void ioniseBox(float *xH, fftwf_complex *densitybox, double *fcoll, double R, int LAST_STEP, int InvMHR); //assigns ionisation level of xH box according to a threshold test

/* MHR00 HELPER FUNCTIONS */

  double dndlmMHR(double M); //co-moving number density in neutral regions between m & m+dm 
  double IntxH(double M);
  void loadHC_A(double &A); //this loads the values of A stored in the relevant file saved in the FZH program Output folder
  void ST_PS_correct(fftwf_complex *densitybox, double M, double *fcoll_box); //corrects for sheth-torman (with Jenkins parameter fits), calculates and returns as a free product the fcoll of each pixel for the given smoothed density field.

  // TO UPDATE VALUES OF PROTECTED MEMBER VARIABLES 
  void setz(double z);
  void setMpix(double M); //M in units of Msol
  void setdelta0(double delta0);
  void setnf(double nf);
  void setP_psiZero(double P_psi);
  void setPsiUnobtain(double Psi);
  void setDeltaIon(float delIon); /* MHR00 */
  void setMuFZH(double Mu);
  void setMomentFZH(int Moment);
  void sethard_code_gen(int hard_code_flag); //this flags whether or not the hard code data has been flagged.
  void setA(double A); //this is the normalisation constant that adjusts the intercept of the GlobMHR model
  void setBtype(int B);
  void setK(double K);
  void setSTPScorr(double STPS);

  // FOR ACCESS TO PROTECTED MEMBER VARIABLES VALUES
  double getz();
  double getMpix();
  double getdelta0();
  double getnf();
  double getsigpix(); // The limits of the integral from eqn29 are 0 to sigpix^2
  double getP_psiZero(); // Probability of psi=0 (i.e. pixel ionised)
  double getPsiUnobtain(); //Returns the value of Psi above which is unobtainable for this redshift (stops wasted calculations being done, especially important if you're running integrals on the PDF)
  float getDeltaIon(); /* MHR00 */
  double getMuFZH();
  int getMomentFZH();
  int getPpsi0(double &PpsiZERO); //this returns the hard coded (HC) value of the probability at psi(0) found at an earlier point using normPDF
  int getPsiMax(double &PsiMx); //returns hard coded value of Psi Max
  int getDeltaMax(double &DeltaMx); //returns hard coded value of Delta Max 
  int gethard_code_gen();
  double getA();
  int getBtype();
  double getK();
  double getSTPScorr();

 protected: 
  /*protected variables can be accessed by other members of the 
    class but are protected from external interference*/
  Cosmology *C;
  Tocmfast *T0;
  FILE *DBUG;
  double z1, nf1, K1;
  int Btype;
  // FZH specific variables:
  double PpsiZero[40],PsiMax[40], DeltaMax[40], Ppsi0, deltaZero, PsiUnobtain, MUFZH;  
  double MPIX;
  int MOMENT, HARD_CODE_GEN;
  // MHR specific variables:
  float DELTAION; 
  double STPScorr, Aa, Astore[40];

};

/*Dummy functions calling a class member functions for integration 
  or differentiation*/
double dummyP_Psi(double psi);
double setP_Psi(double psi, Reionise *R1, int iflag);
double dummyPcross(double sigmaM2);
double setPcross(double sigmaM2, Reionise *R1, int iflag);
double dummypsiFromDelta(double deltaM);
double setpsiFromDelta(double deltaM, Reionise *R1, int iflag);
double dummyPpsiPionInt(double deltaM);
double setPpsiPionInt(double deltaM, Reionise *R1, int iflag);

/*
  Functions for gsl based integrations
*/
double intLinBias(double mass, void *p);
struct linBias_params {Reionise *r1;};
double intDenCorr(double k, void *p);
struct DenCorr_params {Cosmology *c1; double r12; double z1;};

//NEUTRAL FRACTION dummies/sets
double setIntQbar(double M, Reionise *R1, int iflag);
double dummyIntQbar(double M);

//STATISTICS integral dummies/sets
double dummyIntMuFZH(double psi); 
double setIntMuFZH(double psi, Reionise *R1, int iflag); 
double dummyIntMomentFZH(double psi);
double setIntMomentFZH(double psi, Reionise *R1, int iflag);

//MHR integral dummies/sets
double setIntxH(double M, Reionise *R1, int iflag);
double dummyIntxH(double M);

/*********************************************************************
 * MHR00 HELPER FUNCTION
 ********************************************************************/
int compare_flts(const void* a, const void* b);

/*********************************************************************
General maths
*********************************************************************/
double sinc(double x);

#endif

