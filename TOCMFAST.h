/* TOCMFAST.h (CAW2012)
 *
 * This adapts much of the data box manipulation &
 * statistical analysis from 21cmfast into class routines.
 *
 * Also performs Other statistical analysis on boxes from 21CMFAST-like
 * programs that was not included in the original program
 *
 * Development notes:
 * Eventually would like this to run independenly of ANAL_PARAMS and INIT_PARAMS
 *
 * Do I want to remove the option of converting between
 * DELTA and delta (although note that we would need to adapt MHR runs 
 * to work with and export delta, which was not what we did in our original run.
 */


#ifndef _TOCMFAST_H
#define _TOCMFAST_H

#include "../Cosmo/dcosmology.h"
#include "../NR/dnumrecipes.h"
#include <complex.h>
#include <fftw3.h>

#include "ANAL_PARAMS.H" /*Should be the 21CMFAST header used in generating 
			  *the density fields under analysis */
#include "INIT_PARAMS.H" /*Should be the 21CMFAST header used in generating 
			  *the density fields under analysis */

/************************************************************************
USER CHANGEBLE FLAGS 

!!!! Nb. IF YOU CHANGE THESE YOU MUST make clean THEN REMAKE !!!!

Nb: This Class requires flags that are contained within ANAL_PARAMS.H
from 21cm FAST. Make sure to include that relevant to 
the box under analysis
The following variable is used throughout and takes the below options:
boxtype is important for making sure the boxes are treated
correctly (as delta is padded). 
   1=xH, 
   2=dT, 
   3=density 
************************************************************************/

/*******************************************
 * CHOOSE IF WANT TO MODEL INSTRUMENTAL EFFECTS
*******************************************/
const int model_inst (1); 
const int instNEW_DIM (150);

/*These two options regard the resampling used when modeling instrumental.
They have two uses:

(1) If using it in TOCMstat to approximate instrument resolution box_len/new_dim should roughly give you a telescopes resolution power.
(2) If using in TOCMSubsample, then this is the number of pixels per side of the sub sampled population of boxes.
 
Note for simplicities sake the programs require that HII_DIM/NEW_DIM has to be an integer for both applications
*/
												   
//NOTE skewVskew should really not be in TOCMFAST, should be in USERFLAGS. need to make work round for this.
#define smoothVskew (int) 0 //Do you wnat to consider the skew as a function of smoothing scale

#define SUB_DIM (HII_DIM/instNEW_DIM)

/***********************************************************************
 * HAVE THE DENSITY BOXES BEEN SMOOTHED PREVIOUSLY? e.g.MHR output delta boxes
 **********************************************************************/
const int SmoothedDensityBox (0); //If the density box under examination has been smoothed previously then it is no longer a FFT padded realbox and this flag needs to be made 1 so that the program reacts accordingly.

/************************** Data box variables *************************/
const int USE_TS_IN_21CM (0); /* In the run your analysing, was Ts included as a variable (check HEAT_PARAMS for given run): Yes ->  1  No ->  0  */
const int DELTA (0);
/* 1 Convert the DELTA = rho/<rho> to be delta = rho/<rho>-1
   0 Leave it in its default state which should be i.e. delta = rho/<rho>-1 (defualt for e.g. 21CMFAST)*/

/************ Variables to adjust the behaviour of histobox**************/
const float bin_factor (1.05);
const float bin_first_bin_ceil (0.01);
const float bin_max (1000.0);	

/******** number of samples for bootstrap *********/
const unsigned int BOOTSAMPLE (150);

/******************* Instrumental details *********************/
/*** NOTE REDUNDANT UNTIL NOISE SAMPLER COMPLETED**************/
#define Atot (float) 2752.0 // m^2 effective area of total telescope
#define int_time (float) 1000.0 // Integration time of observation in hours
	      

/****************** CONSTANTS, DO NOT ALTER PLEASE!! ********************/
const double TWOPI (2.0*PI);
const double E (2.71828182846);
const double SPEEDOFLIGHT_KMS (299792.5); //speed of light in km/s
const double nu_0 (1420.40575177); //rest frequency of 21cm line in MHz

/************************* Tocmfast class *******************************/
class Tocmfast {

 public:
 /***************************************************
                     INITIALISATION
  ***************************************************/
  Tocmfast(FILE *LOG, Cosmology *c1);
  ~Tocmfast();

  /***************************************************
          ROUTINES CONTROLLING IN/OUT OF BOXES
  ***************************************************/
  // importbox -> imports data from 21CMFAST REAL box (of type boxtype) in *Fname to *box.
  int importbox(char *Fname, float *box, int boxtype, int Res); 

  // exportBox -> Exports an xH box to file*/
  void exportBox(float *box, double nf, double z, int boxtype); 

  // copyBox -> Makes copy of *source box  arrays in *target 
  void copyBox(float *target, float *source); 

  int variXtract(char *arg, int boxtype, int &Res, float &L);
  long long unsigned real1DIndex(int x, int y, int z, int Res); 

  /***************************************************
       ROUTINES FOR STATISTICAL ANALYSIS OF BOXES
  ***************************************************/ 
  int resamplebox(char *argv, float *realbox, float *samplebox, int boxtype); //smoothes & resamples according to the dimensions specified above
  int smoothbox(char *file, float *realbox, float R, int boxtype); // smoothbox -> Smoothed input box, can be used to import a file by setting R=0 (currently only set up for density field) 

  // TWO-POINT STATISTCS
  int PowerSpec(float *box1, float *box2, int boxtype1, int boxtype2, int dimensional); // PowerSpec -> Calculates the power spectrum of two different fields. boxtype is the   same as for PowerSpec. Adapted from 21CMFAST. 
  /* dimensional = 1 if you want to make a dimensional power spec i.e. <box>*P_delboxdelbox
     dimensional = 0 if you want to make a dimensionless power spec i.e. <box>*P_delboxdelbox */ 
  int corrbox(float *box1, float *box2, int boxtype1, int boxtype2); //calculates the spatial correlation function of two boxes, pass the same box if you wish to calculate auto-correlation. 
  int corrbox_full(float *box1, float *box2, int boxtype1, int boxtype2); //calculates the spatial correlation function of two boxes, pass the same box if you wish to calculate auto-correlation. This is the "proper method" that calculates for a properly padded box. Takes FOREVER, so made corrbox which doesn't bother with padding.  
  //double Rxyz(double i, double j, double k, double x, double y, double z); //returns the euclidean separation of two points in 3D. 
  int Rxyz2(int sep1, int sep2, int sep3); //returns the euclidean separation of two points in 3D. 
  int histobox(float *box, int boxtype); // histobox-> Makes a histogram of a box   
  double moments_w_err(float *box, int boxtype, int nonZero, double &kurt_Sig2, double &skew_Sig2, double &skew_norm_Sig2, double &Sig2_Sig2, double &kurt_avg, double &skew_avg, double &skew_norm_avg, double &Sig2_avg); //calculates the skew and normalised skew with errors estimated by sampling random instrumental noise
  double moments(float *box, int boxtype, int nonZero, int n);
  int subsample(float *sample, int boxtype, int x, int y, int z, float *box);
  void bootstrap(float *box, int boxtype, double *mean_stat, double *std_stat);



   /***************************************************
       HELPER ROUTINES
  ***************************************************/
  float noise_Sig(); //defines the variance of the gaussian noise
  void HII_filter(fftwf_complex *box, int filter_type, float R);

  double averagebox(float *box, int boxtype, int nonZero); /*Calulate the average value of box*/

  const char* boxstring(int boxtype); /*Retruns a string for filenmaes etc according to the boxtype*/
  void check_region(float * box, int dimensions, float Rsq_curr_index, int x, int y, int z, int x_min, int x_max, int y_min, int y_max, int z_min, int z_max);//lifted from 21cmfast
  void update_in_sphere(float * box, int dimensions, float R, float xf, float yf, float zf, int InvMHR); //lifted from 21cmfast
/*********************************************************************
 FOR UPDATING/ACCESSING VALUES OF PROTECTED MEMBER VARIABLES 
*********************************************************************/
  // UPDATE:-
  void setz(double z);
  void setnf(double nf);
  void setR(double R);
  void setZeta(double Zeta);
  void setave(double avg);
  void setSig2(double sigma2);
  void setNewDim(int NewDim);
  void setDIMstore(int DimStore);

  // ACCESS:-
  double getz();
  double getnf();
  double getR();
  double getZeta();
  double getave();
  double getSig2();
  double getRes(); //uses the hard coded values of MWA_Instrumentals to return angular resolution.
  //int getNEW_R_INDEX(int x,int y,int z);
  int getNEW_TOT_NUM_PIXELS();
  int getDIMstore();
 protected: 
//protected variables can be accessed by other members of the class but are protected from external interference
  Cosmology *c;
  FILE *DBUG;
  FILE *F; /* File read in and box data storage: */
  double z1, nf1, R1, Zeta1, avg1, SiGma21;
  int NEW_DIM, DIM_STORE; 
}; 

int import_as_fft(char *file, int res, int boxtype, fftwf_complex *box_fft);

/*********************************************************************
Macros for indexing etc for the resampled box
*********************************************************************/
#define NEW_D (unsigned long long) instNEW_DIM
#define NEW_MIDDLE (instNEW_DIM/2)
#define NEW_MID ((unsigned long long)NEW_MIDDLE)

#define NEW_TOT_NUM_PIXELS (unsigned long long)(NEW_D*NEW_D*NEW_D)
#define NEW_TOT_FFT_NUM_PIXELS ((unsigned long long)(NEW_D*NEW_D*2llu*(NEW_MID+1llu)))
#define NEW_KSPACE_NUM_PIXELS ((unsigned long long)(NEW_D*NEW_D*(NEW_MID+1llu)))

/* INDEXING MACROS NOTE I THINK CAN JUST USE USUAL ONES INSTEAD*/
#define NEW_C_INDEX(x,y,z)((unsigned long long)((z)+(NEW_MID+1llu)*((y)+NEW_D*(x))))// for 3D complex array
#define NEW_R_FFT_INDEX(x,y,z)((unsigned long long)((z)+2llu*(NEW_MID+1llu)*((y)+NEW_D*(x)))) // for 3D real array with the FFT padding
#define NEW_R_INDEX(x,y,z)((unsigned long long)((z)+NEW_D*((y)+NEW_D*(x)))) // for 3D real array with no padding

/*********************************************************************
 Wrapper functions for the std library functions fwrite and fread
 which should improve stability on certain 64-bit operating systems
 when dealing with large (>4GB) files - LIFTED FROM 21CMFAST
*********************************************************************/
size_t mod_fwrite (const void *, unsigned long long, unsigned long long, FILE *);
size_t mod_fread(void * array, unsigned long long size, unsigned long long count, FILE * stream);

#endif
